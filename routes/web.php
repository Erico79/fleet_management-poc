<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/load-route-data/{route}', 'HomeController@loadRoute')->name('load-route');
Route::get('/profile', 'Auth\UserProfileController@myProfile')->name('my-profile');
Route::get('export', 'Registration\DriversController@export');
Route::get('import', 'Registration\DriversController@import');
Route::get('profile/{user}', 'Auth\UserProfileController@show')->name('edit-profile');
Route::put('profile/{id}', 'Auth\UserProfileController@update')->name('update-profile');

Route::group(['middleware' => ['auth', 'check.permission'], 'group-name' => 'system-admin'], function() {
    Route::get('companies', 'Registration\CompaniesController@index')->name('manage-companies');
    Route::get('companies-dt', 'Registration\CompaniesController@all')->name('companies-dt');
    Route::get('company/{company}', 'Registration\CompaniesController@show')->name('get-company');
    Route::post('company', 'Registration\CompaniesController@store')->name('save-company');
    Route::delete('company/{id}', 'Registration\CompaniesController@destroy')->name('delete-company');
    Route::put('company', 'Registration\CompaniesController@update')->name('update-company');
});

Route::group(['middleware' => ['auth', 'check.permission'], 'group-name' => 'company-admin'], function() {
    // company admins
    Route::get('company-admins', 'Registration\CompaniesAdminController@index')->name('manage-company-admins');
    Route::get('company-admins-dt', 'Registration\CompaniesAdminController@all')->name('company-admins-dt');
    Route::get('Company-admin/{user}', 'Registration\CompaniesAdminController@show')->name('get-company-admin');
    Route::post('company-admin', 'Registration\CompaniesAdminController@store')->name('save-company-admin');
    Route::delete('user/{id}', 'Registration\CompaniesAdminController@destroy')->name('delete-company-admin');
    Route::put('company-admin', 'Registration\CompaniesAdminController@update')->name('update-company-admin');
    //routes
    Route::get('routes', 'RoutesController@index')->name('routes');
    Route::get('routes-dt', 'RoutesController@routesDT')->name('route-dt');
    Route::get('route/{route}', 'RoutesController@show')->name('edit-route');
    Route::post('route', 'RoutesController@store')->name('save-route');
    Route::delete('route/{id}', 'RoutesController@destroy')->name('delete-route');
    Route::put('route/{id}', 'RoutesController@update')->name('update-route');
    //driversRegis
    Route::get('drivers', 'Registration\DriversController@index')->name('drivers');
    Route::get('drivers-dt', 'Registration\DriversController@driversDT')->name('driver-dt');
    Route::get('driver/{user}', 'Registration\DriversController@show')->name('edit-driver');
    Route::post('driver', 'Registration\DriversController@store')->name('save-driver');
    Route::delete('driver/{id}', 'Registration\DriversController@destroy')->name('delete-driver');
    Route::put('driver/{id}', 'Registration\DriversController@update')->name('update-driver');
    Route::post('import-drivers','Registration\DriversController@import')->name('import-drivers');
    Route::get('download-drivers-template', 'Registration\DriversController@downloadCsvTemplate')->name('drivers-import-template');
    // payments
    Route::get('route-payments-dt/{route_id}', 'PaymentsController@routePayments')->name('route-payments-dt');
    Route::get('chart-data/{route_id}', 'PaymentsController@chartData')->name('chart-data');
    //vehicles
    Route::get('vehicles', 'VehiclesController@index')->name('vehicles');
    Route::get('vehicles-dt', 'VehiclesController@vehiclesDT')->name('vehicle-dt');
    Route::get('vehicle/{vehicle}', 'VehiclesController@show')->name('edit-vehicle');
    Route::post('vehicle', 'VehiclesController@store')->name('save-vehicle');
    Route::delete('vehicle/{id}', 'VehiclesController@destroy')->name('delete-vehicle');
    Route::put('vehicle/{id}', 'VehiclesController@update')->name('update-vehicle');
    Route::get('route-vehicles/{route}', 'VehiclesController@routeVehicles')->name('load-vehicles');
    Route::get('route-vehicle/payments/{vehicle}', 'VehiclesController@vehiclePayments')->name('vehicle-payments-dt');
    Route::post('import-vehicles', 'VehiclesController@import')->name('import-vehicles');
    Route::get('download-vehicles-template', 'VehiclesController@downloadCsvTemplate')->name('vehicles-import-template');
    // users
    Route::put('reset-password/{user}', 'Auth\UserProfileController@resetPassword')->name('reset-password');
});

Route::group(['middleware' => ['auth', 'check.permission'], 'group-name' => 'driver'], function() {
    // payments received from driver
    Route::get('driver-payments', 'PaymentsController@driverPayments')->name('driver-payments');
    Route::get('driver-payments-dt', 'PaymentsController@driverPaymentsDT')->name('driver-payments-dt');
});
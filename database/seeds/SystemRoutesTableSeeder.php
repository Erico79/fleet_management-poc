<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use App\SystemRoute;
use App\Role;

class SystemRoutesTableSeeder extends Seeder
{
    const SystemAdmin = 'system-admin';
    const CompanyAdmin = 'company-admin';
    const Driver = 'driver';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collection = \Illuminate\Support\Facades\Route::getRoutes();
        foreach ($collection as $route) {
            $action = $route->getAction();

            if ($action['prefix'] != 'api' && $action['prefix'] != 'oauth') {
                if (isset($action['group-name'])) {
                    $group_name = $action['group-name'];

                    switch ($group_name) {
                        case self::SystemAdmin:
                            $this->createRoutes($route, Role::systemAdmin()->id);
                            break;

                        case self::CompanyAdmin:
                            $this->createRoutes($route, Role::companyAdmin()->id);
                            break;

                        case self::Driver:
                            $this->createRoutes($route, Role::driver()->id);
                            break;
                    }
                }
            }
        }
    }

    private function createRoutes($route, $role_id)
    {
        $action = $route->getAction();

        DB::transaction(function() use($route, $action, $role_id) {
            // create route
            $system_route = SystemRoute::create(['url' => $route->uri, 'name' => $action['as']]);

            // assign route to role
            $system_route->roles()->attach($role_id);
        });
    }
}

<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->first_name = 'Nicholas';
        $user->surname = 'Njuguna';
        $user->email = 'nicko.njuguna@gmail.com';
        $user->phone_no = '254728567895';
        $user->id_no = '22554455';
        $user->password = bcrypt(env('SYS_ADMIN_PASS'));
        $user->role_id = Role::systemAdmin()->id;
        $user->save();
    }
}

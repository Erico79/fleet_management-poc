<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['code' => Role::SystemAdmin, 'name' => 'System Admin']);
        Role::create(['code' => Role::CompanyAdmin, 'name' => 'Company Admin']);
        Role::create(['code' => Role::Driver, 'name' => 'Driver']);
    }
}

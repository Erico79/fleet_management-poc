<?php

use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company_id = \App\Company::first()->id;

        // routes
        factory(\App\Route::class, 20)->create(['company_id' => $company_id]);

        // drivers
        factory(\App\User::class,10)->create([
            'role_id' => \App\Role::driver()->id,
            'company_id' => $company_id
        ])->each(function($u) use($company_id) {
            factory(\App\Driver::class, 500)->create([
                'user_id' => $u->id
            ])->each(function($driver) use($company_id) {
                // vehicles
                factory(\App\Vehicle::class)->create([
                    'driver_id' => $driver->id,
                    'company_id' => $company_id,
                    'created_by' => \App\Role::companyAdmin()->users()->first()->id
                ]);
            });
        });

        // payments
        factory(\App\Payment::class, 1000)->create();
    }
}

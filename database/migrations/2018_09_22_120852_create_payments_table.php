<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('registration_no',20);
            $table->double('amount');
            $table->string('customer_phone_no',50)->comment('should be picked from msisdn from nicks mpesa');
            $table->text('payment_details')->nullable();
            $table->boolean('status')->default(false);
            $table->string('business_short_code')->comment('from mpesa table given by nick');
            $table->string('trans_time')->comment('from mpesa table given by nick');
            $table->string('trans_type')->comment('from mpesa table given by nick');
            $table->string('trans_id')->comment('from mpesa table given by nick');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}

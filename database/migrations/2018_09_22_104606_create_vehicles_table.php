<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('registration_no', 150)->unique();
            $table->integer('capacity');
            $table->integer('route_id')->unsigned();
            $table->integer('driver_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->boolean('active')->default(true);
            $table->foreign('route_id')
                ->references('id')->on('routes')
                ->onUpdate('cascade')->onDelete('no action');
            $table->foreign('driver_id')
                ->references('id')->on('drivers')
                ->onUpdate('cascade')->onDelete('no action');
            $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onUpdate('cascade')->onDelete('no action');
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')
                ->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}

<?php
/*-
- masterfile_id (fk to masterfiles)
- owner_masterfile_id (fk to masterfiles - should be the company that has hired the driver)
- license_ref_no (should be unique*/

use App\BusinessRole;
use Faker\Generator as Faker;

$factory->define(App\Driver::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return \App\User::first()->id;
        },
        'membership_no' => $faker->unique()->randomNumber(8),
    ];
});

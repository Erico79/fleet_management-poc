<?php

use App\Vehicle;
use Faker\Generator as Faker;

$factory->define(App\Payment::class, function (Faker $faker) {
    return [
        'registration_no' => function() {
            return \App\Vehicle::all()->shuffle()->pluck('registration_no')->first();
        },
        'amount' => rand(1000, 50000),
        'customer_phone_no' => '2547' . $faker->unique()->randomNumber(8),
        'status' => 1,
        'business_short_code' => '001225',
        'trans_time' => $faker->time(),
        'trans_type' => 'Paybill',
        'trans_id' => $faker->unique()->randomNumber(5)
    ];
});

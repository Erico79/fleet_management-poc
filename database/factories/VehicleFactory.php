<?php

use Faker\Generator as Faker;

$factory->define(\App\Vehicle::class, function (Faker $faker) {
    return [
        'registration_no' => 'KBA' . $faker->unique()->randomNumber(3),
        'route_id' => function() {
            return \App\Route::all()->shuffle()->pluck('id')->first();
        }
    ];
});

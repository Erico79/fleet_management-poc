<?php
/*- name (this can be either the company name or customer name or driver's name)
- id_no (string - should be nullable for now)
- business_role_id (integer should be fk to business_roles table)
- registration_date (date)
- phone_no (should be unique)
- owner_masterfile_id (fk to masterfiles)*/

use App\BusinessRole;
use Faker\Generator as Faker;

$factory->define(App\Masterfile::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'id_no' => $faker->unique()->randomNumber(8),
        'business_role_id' => function() {
            return BusinessRole::driver()->id;
        },
        'registration_date' =>$faker->date(),
        'phone_no' => $faker->phoneNumber,
        'owner_masterfile_id' => function() {
            return BusinessRole::owner()->masterfiles()->first()->id;
        }
    ];
});

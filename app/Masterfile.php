<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Masterfile extends Model
{
    protected $guarded = ['id'];

    public function drivers() {
        return $this->hasMany(Masterfile::class, 'owner_masterfile_id', 'id');
    }

    public function driver() {
        return $this->hasOne(Driver::class);
    }

    public function owner() {
        return $this->belongsTo(Masterfile::class, 'owner_masterfile_id', 'id');
    }

    public function businessRole() {
        return $this->belongsTo(BusinessRole::class);
    }

    public function user() {
        return $this->hasOne(User::class);
    }

    public function routes() {
        return $this->hasMany(Route::class);
    }

    public function vehicles() {
        return $this->hasMany(Vehicle::class);
    }
}

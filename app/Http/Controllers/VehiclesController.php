<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Imports\VehiclesImport;
use App\Payment;
use App\Repositories\Company;
use App\Driver;
use App\Repositories\Registration;
use App\Role;
use App\Route;
use App\Vehicle;
use Illuminate\Http\Request;
use App\Repositories\Company as CompanyRepo;
use Illuminate\Database\QueryException;
use Yajra\DataTables\Facades\DataTables;

class VehiclesController extends Controller
{
    /**
     *
    $routes =CompanyRepo::authCompanyAdmin()->routes;
    $drivers =CompanyRepo::authCompanyAdmin()->drivers;
    $vehicles=CompanyRepo::authCompanyAdmin()->vehicles;
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $routes = auth()->user()->company->routes;
        $drivers = Company::authCompanyAdmin()->company->users()->where('role_id', Role::driver()->id)->get();
        $vehicles = auth()->user()->company->vehicles;

        return view('system-admin.vehicles')->with([
            'routes'=>$routes,
            'drivers'=>$drivers,
            'vehicles'=> $vehicles
        ]);
    }


    public function store(Request $request)
    {
        $data = request()->validate([
            'registration_no' => 'required',
            'capacity' => 'required',
            'route_id' => 'required',
            'driver_id' => 'required',
            'active' => 'required',
        ]);
        $data['company_id'] = auth()->user()->company_id;
        $data['created_by'] = auth()->user()->id;
        $data['registration_no'] = str_replace(' ', '', request('registration_no'));
        $capacity= request('capacity');

        if (Vehicle::where('registration_no', $data['registration_no'])->count()) {
            request()->session()->flash('warning', 'The Registration ('. $data['registration_no'] .') already exists!');
            return redirect()->route('vehicles');
        }

        if($capacity>50){
            request()->session()->flash('warning', 'The capacity should not exceed 50!');
            return redirect()->route('vehicles');
        }
        Vehicle::create($data);

        request()->session()->flash('success', 'Vehicle  has been added.');
        return redirect()->route('vehicles');
    }

    public function show(Vehicle $vehicle)
    {
        $response = $vehicle;
        $response['route_id']=$vehicle->route->id;
        $response['driver_id']=$vehicle->driver->id;

        return response()->json($response);
    }

    /**
    $response = $user;
    $response['membership_no'] = $user->driver->membership_no;
    return response()->json($response);
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function vehiclesDT()
    {
        $vehicles = auth()->user()->company->vehicles;

        return DataTables::of($vehicles)
            ->editColumn('route_id',function ($vehicle){
                return $vehicle->route->from.' '.$vehicle->route->to;
            })
            ->editColumn('driver_id',function ($vehicle) {
                $user = $vehicle->driver->user;
                return $user->first_name.' '.$user->middle_name.' '.$user->surname;
            })
            ->editColumn('active',function ($vehicle){
                return $vehicle->active ? 'Inactive' : 'Active';
            })

            ->addColumn('actions', function ($vehicle) { // add custom column
                $actions = '<div class="pull-right"><button source="' . route('edit-vehicle', $vehicle->id) . '" class="btn btn-warning btn-sm edit-vehicle-btn" vh-id="' . $vehicle->id . '"><i class="fa fa-edit"></i> Edit</button>';
                $actions .= '<form action="' . route('delete-vehicle', $vehicle->id) . '" style="display: inline;" method="post" class="del_vehicle_form">';
                $actions .= method_field('DELETE');
                $actions .= csrf_field() . '<button class="btn btn-danger btn-sm">Delete</button></form>';
                $actions .= '</div>';
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function update()
    {
        $id = request('id');
        $data = request()->validate([
            'registration_no' => 'required',
            'route_id' => 'required',
            'driver_id' => 'required',
            'capacity' => 'required',
            'active' => 'required',
            'id' => 'required|numeric'
        ]);
        $data['registration_no'] = str_replace(' ', '', request('registration_no'));
        if (Vehicle::where('registration_no', $data['registration_no'])->where('id', '<>', $id)->count()) {
            request()->session()->flash('warning', 'The Registration ('. $data['registration_no'] .') already exists!');
            return redirect()->route('vehicles');
        }
        $capacity= request('capacity');
        if($capacity>50){
            request()->session()->flash('warning', 'The capacity should not exceed 50!');
            return redirect()->route('vehicles');
        }
        Vehicle::where('id',$id )->update($data);

        request()->session()->flash('success', 'Vehicle has been updated.');
        return redirect()->route('vehicles');
    }


    public function destroy($id)
    {
        try {
            Vehicle::destroy($id);

            request()->session()->flash('success', 'Vehicle has been deleted.');
        } catch (QueryException $qe) {
            request()->session()->flash('warning', 'Could not delete vehicle because it\'s being used in the system!');
        }

        return redirect()->route('vehicles');

    }

    public function routeVehicles(Route $route) {
        $from_date = $_GET['from_date'];
        $to_date = $_GET['to_date'];
        $reg_no = null;
        $no_of_records = 10;

        if (isset($_GET['reg_no']) && !empty($_GET['reg_no']))
            $reg_no = $_GET['reg_no'];

        if (isset($_GET['no_of_records']) && !empty($_GET['no_of_records']))
            $no_of_records = $_GET['no_of_records'];

        if ($reg_no) {
            $reg_nos = $route->vehicles()
                ->where('registration_no', 'like', '%' . $reg_no . '%')
                ->pluck('registration_no')->toArray();
        } else {
            $reg_nos = $route->vehicles()->pluck('registration_no')->toArray();
        }
        $route_total = Payment::whereIn('registration_no', $reg_nos)
            ->whereBetween('created_at', [$from_date, $to_date])
            ->sum('amount');

        if ($no_of_records != 'all') {
            if ($reg_no) {
                $route_vehicles = $route->vehicles()
                    ->where('registration_no', 'like', '%' . $reg_no . '%')
                    ->paginate($no_of_records);
            } else {
                $route_vehicles = $route->vehicles()->paginate($no_of_records);
            }
        } else {
            if ($reg_no) {
                $route_vehicles = $route->vehicles()
                    ->where('registration_no', 'like', '%' . $reg_no . '%')
                    ->get();
            } else {
                $route_vehicles = $route->vehicles;
            }
        }

        return view('companyadmin.partials.vehicle-section', [
            'route_vehicles' => $route_vehicles,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'route_total' => $route_total,
            'reg_no' => $reg_no,
            'rows' => [
                10 => '10 records',
                25 => '25 records',
                35 => '35 records',
                'all' => 'All records',
            ],
            'no_of_records' => $no_of_records
        ]);
    }

    public function vehiclePayments(Vehicle $vehicle) {
        $reg_no = $vehicle->registration_no;
        $payments = Payment::where('registration_no', $reg_no)
            ->whereBetween('created_at', [$_GET['from_date'], $_GET['to_date']])
            ->get();
        $vehicle_total = Payment::where('registration_no', $reg_no)
            ->whereBetween('created_at', [$_GET['from_date'], $_GET['to_date']])
            ->sum('amount');

        return DataTables::of($payments)
            ->editColumn('id', function($payment) {
                return '<div id="'. $payment->id . '" class="avatar">
                            <img class="img-avatar"
                                 src="'. asset('images/money.svg') . '"
                                 style="width: 36px; height: 40px"/>
                        </div>';
            })
            ->addColumn('driver', function($payment) {
                $driver = Vehicle::where('registration_no', $payment->registration_no)->first()->driver;
                $user = $driver->user;
                return '<div>' . $user->first_name . ' ' . $user->middle_name . ' ' . $user->surname .' </div>
                        <div class="small text-muted">Phone No: '. $user->phone_no .'</div>';
            })
            ->editColumn('amount', function($payment) use($vehicle_total) {
                $percentage = round(($payment->amount / $vehicle_total) * 100);

                return '<div class="clearfix">
                            <div class="float-left">
                                <strong>Ksh. '. number_format($payment->amount,2) .'</strong>
                            </div>
                            <div class="float-right">
                                <small class="text-muted">('. $percentage .'%)</small>
                            </div>
                        </div>
                        <div class="progress progress-xs">
                            <div class="progress-bar bg-success" role="progressbar" style="width: '. $percentage .'%" 
                            aria-valuenow="'. $percentage .'" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>';
            })
            ->editColumn('created_at', function($payment) {
                return '<strong>'. date('Y-m-d H:i', strtotime($payment->created_at)) .'</strong>
                        <div class="small text-muted">'. $payment->created_at->diffForHumans() .'</div>';
            })
            ->rawColumns(['id', 'driver', 'amount', 'created_at'])
            ->make();
    }

    public function import()  {
        // upload the file
        if (request()->has('vehicles_import')) {
            $extension = request('vehicles_import')->getClientOriginalExtension();
            $file_name = 'import_vehicles' . time() . '.' . $extension;
            $path = request('vehicles_import')->storeAs('public', $file_name);

            // read the file and get data
            $array = (new VehiclesImport())->toArray($path);
            $vehicle_rows = $array[0];

            // create drivers and their respective accounts
            for ($i = 1; $i < count($vehicle_rows); $i++) {
                $vehicles = $vehicle_rows[$i];

                if (
                    Driver::where('membership_no', $vehicles[2])->count() &&
                    Vehicle::where('registration_no', $vehicles[0])->count() === 0 &&
                    $vehicles[4] <= 50 &&
                    auth()->user()->company->routes()->where('id', $vehicles[1])->count() === 1
                ) {
                    $driver = Driver::where('membership_no', $vehicles[2])->first();
                    $info['registration_no'] = $vehicles[0];
                    $info['route_id'] = $vehicles[1];
                    $info['driver_id'] = $driver->id;
                    $info['company_id'] = auth()->user()->company_id;
                    $info['active'] = $vehicles[3];
                    $info['created_by'] = auth()->user()->id;
                    $info['capacity'] = $vehicles[4];

                    Vehicle::create($info);
                }
            }

            request()->session()->flash('info', 'Completed vehicles import.');
            return redirect()->route('vehicles');
        }
    }

    public function downloadCsvTemplate() {
        return response()->download(storage_path('app/vehicles.xlsx'));
    }
}

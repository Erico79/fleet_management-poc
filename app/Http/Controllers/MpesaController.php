<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Repositories\SMS;
use App\Vehicle;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MpesaController extends Controller
{
    private $_errors = [];

    public function record() {
        $validator = Validator::make(request()->all(), [
            'registration_no' => 'required',
            'amount' => 'required|numeric',
            'customer_phone_no' => 'required',
            'business_short_code' => 'required',
            'trans_time' => 'required',
            'trans_type' => 'required',
            'trans_id' => 'required',
        ]);

        $payment_data = request([
            'registration_no',
            'amount',
            'customer_phone_no',
            'business_short_code',
            'trans_time',
            'trans_type',
            'trans_id',
            'payment_details'
        ]);
        $payment_data['registration_no'] = str_replace(' ', '', request('registration_no'));

        $validator->after(function($validator) use($payment_data) {
            $exists = Vehicle::where('registration_no', $payment_data['registration_no'])->count();
            if (!$exists) {
                $validator->errors()
                    ->add(
                        'registration_no',
                        'The registration_no('. request('registration_no') .') is not registered in the system!)'
                    );
            }
        });

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ];
            return response()->json($response);
        } else {
            try {
                Payment::create($payment_data);
            } catch (QueryException $qe) {
                $this->_errors[] = $qe->getMessage();
            }

            if (count($this->_errors)) {
                $response = ['success' => false, 'error' => $this->_errors];
                return response()->json($response);
            } else {
                $driver = Vehicle::where('registration_no', $payment_data['registration_no'])->first()->driver->user;

                // send sms to the company
                $sms = new SMS();
                $sms->setRecipients($driver->phone_no);
                $sms->setMessage($this->message($payment_data));
                $sms->send();

                return response()->json([
                    'success' => true,
                    'message' => 'Payment has been recorded'
                ]);
            }

        }
    }

    private function message($payment_data) {
        $message = "Hi. A customer has made a payment via MPesa. The following are the details;\n"
            . "Customer Phone: " . $payment_data['customer_phone_no'] . "\n"
            . "Amount Paid: " . $payment_data['amount'] . "\n"
            . "Vehicle Reg. No: " . $payment_data['registration_no'] . "\n";

        return $message;
    }
}

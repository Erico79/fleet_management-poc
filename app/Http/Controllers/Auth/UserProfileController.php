<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 10/20/18
 * Time: 2:41 PM
 */

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Mail\ResetPassword;
use App\Repositories\Registration;
use App\Repositories\SMS;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;


class UserProfileController extends Controller
{

    private $_company, $_user,
        $_errors = [];

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function myProfile() {
        $user = auth()->user();
        $photo_data = $user->photo ? json_decode($user->photo) : [];
        return view('auth.profile', [
            'user' => $user,
            'photo' => isset($photo_data->asset_path) ? $photo_data->asset_path : 'images/avatar.svg'
        ]);
    }
    public function show(User $user) {
        $user_data = $user->toArray();
        $user_data['phone_no'] = ltrim($user_data['phone_no'], '254');
        return $user_data;
    }
    public function update() {
        $id = request('id');

        $validator = Validator::make(request()->all(), [
            'first_name' => 'required',
            'middle_name' => 'required',
            'surname' => 'required',
            'phone_no' => 'required|unique:users,phone_no,' . $id,
            'email' => 'required|unique:users,email,' . $id,
            'id' => 'required|numeric',
            'photo' => 'image|max:5000'
        ],
            [
                'phone_no.unique' => 'The Phone No('. request('phone_no') .') already exists!',
                'id_no.unique' => 'The ID No('. request('id_no') .') already exists!'
            ]
        );

        if ($validator->fails()) {
            $response = ['success' => false, 'errors' => $validator->getMessageBag()->toArray()];
            return response()->json($response);
        } else {
            $info = request(['first_name', 'middle_name', 'surname', 'phone_no', 'email']);
            // upload the image
            if (request()->has('photo')) {
                $path = request('photo')->store('public');
                $path_items = explode('/', $path);
                $info['photo'] = json_encode([
                    'original_path' => $path,
                    'asset_path' => asset('storage/' . end($path_items))
                ]);
            }

            $phone_no = trim(request('phone_no'), '0');
            $info['phone_no'] = '254' . $phone_no;

            try {
                User::where('id', request('id'))->update($info);
            } catch (QueryException $qe) {
                $this->_errors['company_admin'] = 'Encountered an error while updating  the company admin details';
            }
        }

        if (count($this->_errors) === 0) {
            $response = ['success' => true, 'message' => 'Company has been updated'];
        } else {
            $response = ['errors' => $this->_errors, 'success' => false];
        }

        return response()->json($response);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resetPassword(User $user) {
        $password = Registration::generateStrongPassword();
        User::where('id', $user->id)->update(['password' => bcrypt($password)]);

        $sms = new SMS();
        $sms->setRecipients($user->phone_no);
        $sms->setMessage($this->getMessage($user, $password));
        $sms->send();

        Mail::to($user)->send(new ResetPassword($user, $password));

        request()->session()->flash('success', 'The password has been reset and sent to ('. $user->email .')');
        if (request()->has('drivers'))
            return redirect()->route('drivers');
        return redirect()->route('manage-company-admins');
    }

    private function getMessage($user, $password) {
        return "Dear $user->first_name, you password has been reset to $password. "
            . "Click the following link to login; " . url('login');
    }
}
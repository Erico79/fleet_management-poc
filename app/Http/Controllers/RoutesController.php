<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Route;
use Illuminate\Database\QueryException;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Company as CompanyRepo;

class RoutesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('system-admin.routes');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'from' => 'required',
            'to' => 'required'
        ]);

        $company = CompanyRepo::authCompanyAdmin();
        $data['company_id'] = $company->company_id;

        if ($company) {
            if (Route::where('from', request('from'))
                ->where('to', request('to'))
                ->where('company_id', $company->company_id)
                ->count()) {
                request()->session()->flash('warning', 'The route aready exists!');
                return redirect()->route('routes');
            }


            Route::create($data);
            request()->session()->flash('success', 'Route  has been added.');
        }

        return redirect()->route('routes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param Route $route
     * @return Route|\Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Route $route)
    {
            return $route;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function routesDT()
    {
        $routes = auth()->user()->company->routes;

        return DataTables::of($routes)
            ->addColumn('actions', function ($route) { // add custom column
                $actions = '<div class="pull-right"><button source="' .route('edit-route', $route->id). '" class="btn btn-warning btn-sm edit-route-btn" rt-id="' . $route->id . '"><i class="fa fa-edit"></i> Edit</button>';
                $actions .= '<form action="' . route('delete-route', $route->id) . '" style="display: inline;" method="post" class="del_route_form">';
                $actions .= method_field('DELETE');
                $actions .= csrf_field() . '<button class="btn btn-danger btn-sm">Delete</button></form>';
                $actions .= '</div>';
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
    public function update()
    {
        $data = request()->validate([
            'from' => 'required',
            'to' => 'required',
            'id' => 'required|numeric'
        ]);
        $id = request('id');
        $company_id = auth()->user()->company_id;
        if (Route::where('from', request('from'))
            ->where('to', request('to'))
            ->where('company_id', $company_id)
            ->where('id', '<>', $id)->count()) {
                request()->session()->flash('warning', 'The route already exists!');
                return redirect()->route('routes');
        }

        Route::where('id',$id )->update($data);

        request()->session()->flash('success', 'Route has been updated.');
        return redirect()->route('routes');
    }


    public function destroy($id)
    {
        try {
            Route::destroy($id);

            request()->session()->flash('success', 'Route has been deleted.');
        } catch (QueryException $qe) {
            request()->session()->flash('warning', 'Could not delete route because it\'s being used in the system!');
        }

        return redirect()->route('routes');

    }
}

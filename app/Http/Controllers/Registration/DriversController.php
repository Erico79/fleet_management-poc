<?php

namespace App\Http\Controllers\Registration;
use App\Http\Controllers\Controller;
use App\Imports\UsersImport;
use App\Mail\Welcome;
use App\Repositories\Company;
use App\Repositories\Registration;
use App\Repositories\SMS;
use App\Role;
use Illuminate\Http\Request;
use App\User;
use App\Driver;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Company as CompanyRepo;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;

class DriversController extends Controller
{
    private $_user;
    private $_errors = [];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $drivers = CompanyRepo::authCompanyAdmin()->drivers;

        return view('system-admin.registration.drivers')->with(['drivers' => $drivers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(Request $request,SMS $sms)
    {
        request()->validate([
            'first_name' => 'required',
            'middle_name' => 'required',
            'surname' => 'required',
            'id_no' => 'required|unique:users',
            'phone_no' => 'required|numeric|unique:users',
            'email' => 'unique:users',
            'membership_no' => 'required|unique:drivers',
            'photo' => 'image|max:5000'
        ]);

        $info=request(['first_name','middle_name','surname','id_no','email','phone_no']);
        $password = Registration::generateStrongPassword();
        $company_id = auth()->user()->company_id;
        $info['company_id'] = $company_id;
        $info['password']= bcrypt($password);
        $info['role_id'] = Role::driver()->id;
        $phone_no = ltrim(request('phone_no'), '0');
        $info['phone_no'] = '254' . $phone_no;

        if (request()->has('photo')) {
            $path = request('photo')->store('public');
            $path_items = explode('/', $path);
            $info['photo'] = json_encode([
                'original_path' => $path,
                'asset_path' => asset('storage/' . end($path_items))
            ]);
        }

        $this->registerDriver($info, $password, request('membership_no'));

        if (!count($this->_errors)) {
            $sms->setRecipients($info['phone_no']);
            $sms->setMessage($this->message($info, $password));
            $sms->send();

            // email
            Mail::to($this->_user)->send(new Welcome($this->_user, $password));
            request()->session()->flash('success', 'Driver  has been added.');
        } else {
            request()->session()->flash('warning', 'Failed to register driver!');
        }

        return redirect()->route('drivers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(User $user)
    {
        $response = $user;
        $response['membership_no'] = $user->driver->membership_no;
        $response['phone_no'] = ltrim($user->phone_no, '254');
        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function driversDT()
    {
        $users = Role::driver()->users()->where('company_id', auth()->user()->company_id)->get();

        return DataTables::of($users)
            ->addColumn('driver_name', function ($user){
                return $user->first_name.' '.$user->middle_name.' '.$user->surname;
            })
            ->addColumn('membership_no', function($user) {
                return $user->driver->membership_no;
            })
            ->addColumn('actions', function ($user) { // add custom column
                $actions = '<div class="pull-right"><button source="' . route('edit-driver', $user->id) . '" class="btn btn-warning btn-sm edit-driver-btn" dr-id="' . $user->id . '"><i class="fa fa-edit"></i> Edit</button>';
                $actions .= '&nbsp;<form action="' . route('delete-driver', $user->id) . '" style="display: inline;" method="post" class="del_driver_form">';
                $actions .= method_field('DELETE');
                $actions .= csrf_field() . '<button class="btn btn-danger btn-sm">Delete</button></form>&nbsp;'
                    . '<form action="'. route('reset-password', $user->id) . '?drivers' .'" method="post" style="display: inline;">'
                    . csrf_field()
                    . method_field('PUT')
                    . '<button class="btn btn-sm btn-default" 
                                source="' . route('reset-password', $user->id) . '">'
                    . '<i class="fa fa-lock"></i> Reset Password</button></form>';
                $actions .= '</div>';
                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function update()
    {
        $id = request('id');
        $driver = Driver::where('user_id', $id)->first();
        $data = request()->validate([
            'first_name' => 'required',
            'middle_name' => 'required',
            'surname' => 'required',
            'id_no' => 'required|unique:users,id_no,' .$id,
            'phone_no' => 'required|numeric|unique:users,phone_no,' . $id,
            'email' => 'unique:users,email,' .$id,
            'id' => 'required|numeric',
            'photo' => 'image|max:5000',
            'membership_no' => 'required|unique:drivers,membership_no,' .$driver->id

        ]);
        $info=request(['first_name','middle_name','surname','id_no','email','phone_no']);
        $id = request('id');
        $phone_no = trim(request('phone_no'), '0');
        $info['phone_no'] = '254' . $phone_no;
        if (request()->has('photo')) {
            $path = request('photo')->store('public');
            $path_items = explode('/', $path);
            $info['photo'] = json_encode([
                'original_path' => $path,
                'asset_path' => asset('storage/' . end($path_items))
            ]);
        }
        DB::transaction(function() use ($info,$data,$id){
            try {
                $this->_user = User::where('id',$id )->update($info);
            } catch(QueryException $qe) {
                $this->_errors[] = $qe->getMessage();
            }

            try {
                Driver::where('id',$id )->update(['membership_no' => $data['membership_no']]);
            } catch(QueryException $qe) {
                $this->_errors[] = $qe->getMessage();
            }
        });





        request()->session()->flash('success', 'Driver has been updated.');
        return redirect()->route('drivers');
    }


    public function destroy($id)
    {
        try {
            User::destroy($id); // should then delete cascade the driver record associated with the deleted user.

            request()->session()->flash('success', 'Driver has been deleted.');
        } catch (QueryException $qe) {
            request()->session()->flash('warning', 'Could not delete driver because he is being used in the system!');
        }

        return redirect()->route('drivers');

    }

    private function message($info, $password) {
        $message = "Hi, use the following credentials to login into ". route('login') ."\n"
            . "- Phone Number: " . $info['phone_no'] . "\n"
            . "- Password: " . $password;

        return $message;
    }

    public function import(SMS $sms)
    {
        // upload the file
        if (request()->has('drivers_import')) {
            $extension = request('drivers_import')->getClientOriginalExtension();
            $file_name = 'import_drivers_' . time() . '.' . $extension;
            $path = request('drivers_import')->storeAs('public', $file_name);

            // read the file and get data
            $array = (new UsersImport)->toArray($path);
            $driver_rows = $array[0];

            // create drivers and their respective accounts
            for ($i = 1; $i < count($driver_rows); $i++) {
                $drivers = $driver_rows[$i];

                $password = Registration::generateStrongPassword();
                $info['password'] = $password;
                $info['first_name'] = $drivers[0];
                $info['middle_name'] = $drivers[1];
                $info['surname'] = $drivers[2];
                $info['email'] = $drivers[3];
                $info['phone_no'] = $drivers[4];
                $info['id_no'] = $drivers[5];
                $membership_no = $drivers[6];
                $info['company_id'] = auth()->user()->company_id;
                $info['role_id'] = Role::driver()->id;

                if (
                    User::where('email', $info['email'])->count() === 0 &&
                    User::where('phone_no', $info['phone_no'])->count() === 0 &&
                    User::where('id_no', $info['id_no'])->count() === 0
                ) {
                    $this->registerDriver($info, $password, $membership_no);

                    // email/sms their credentials
                    $sms->setRecipients($info['phone_no']);
                    $sms->setMessage($this->message($info, $password));
                    $sms->send();

                    // email
                    Mail::to($this->_user)->send(new Welcome($this->_user, $password));
                }
            }

            request()->session()->flash('info', 'Completed drivers import.');
            return redirect()->route('drivers');
        }
    }

    private function registerDriver($info, $password, $membership_no) {
        DB::transaction(function() use ($info, $password, $membership_no){
            try {
                $this->_user = User::create($info);
            } catch(QueryException $qe) {
                $this->_errors[] = $qe->getMessage();
            }

            try {
                Driver::create(['user_id' => $this->_user->id, 'membership_no' => $membership_no]);
            } catch(QueryException $qe) {
                $this->_errors[] = $qe->getMessage();
            }
        });
    }

    public function downloadCsvTemplate() {
        return response()->download(storage_path('app/drivers.xlsx'));
    }
}

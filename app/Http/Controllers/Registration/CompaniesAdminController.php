<?php

namespace App\Http\Controllers\Registration;

use App\Company;
use App\Mail\Welcome;
use App\Repositories\Registration;
use App\Repositories\SMS;
use App\Role;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Company as CompanyRepo;

class CompaniesAdminController extends Controller
{
    private $_user,
        $_errors = [];

    public function index() {
        return view('system-admin.registration.company-admins');
    }


    public function store(SMS $sms) {

        $validator = Validator::make(request()->all(), [
            'first_name' => 'required',
            'middle_name' => 'required',
            'surname' => 'required',
            'id_no' => 'required|unique:users',
            'phone_no' => 'required|numeric',
            'email' => 'required|unique:users',
            'photo' => 'image|max:5000'
        ],
        [
            'phone_no.unique' => 'The Phone No('. request('phone_no') .') already exists!',
            'email.unique' => 'The Email Address('. request('email') .') already exists!',
            'id_no.unique' => 'The ID No('. request('id_no') .') already exists!'
        ]
        );
        $password = Registration::generateStrongPassword();
        $info = request(['first_name', 'middle_name', 'surname', 'id_no', 'phone_no', 'email']);
        $info['company_id'] = auth()->user()->company_id;
        $info['role_id'] = Role::companyAdmin()->id;
        $info['password']= bcrypt($password);
        $phone_no = trim(request('phone_no'), '0');
        $info['phone_no'] = '254' . $phone_no;

        $validator->after(function($validator) use($info) {
            $exists = User::where('phone_no', $info['phone_no'])
                ->count();
            if ($exists) {
                $validator->errors()
                    ->add(
                        'phone_no',
                        'The Phone No('. $info['phone_no'] .') already exist!)'
                    );
            }
        });

        // upload the image
        if (request()->has('photo')) {
            $path = request('photo')->store('public');
            $path_items = explode('/', $path);
            $info['photo'] = json_encode([
                'original_path' => $path,
                'asset_path' => asset('storage/' . end($path_items))
            ]);
        }
        if (!$validator->fails()) {
            $info['created_by'] = auth()->user()->id;
            // register company admin
            try {
                $this->_user = User::create($info);
            } catch (QueryException $qe) {
                $this->_errors['company_admin'] = 'Encountered an error while registering the company admin.' . $qe->getMessage();
            }
        } else {
            $response = ['success' => false, 'errors' => $validator->getMessageBag()->toArray()];
            return response()->json($response);
        }

        if (count($this->_errors)) {
            $response = ['success' => false, 'errors' => $this->_errors];
        } else {
            // send the Company Admin's credentials to the email/sms
            $sms->setRecipients($info['phone_no']);
            $sms->setMessage($this->message($info, $password));
            $sms->send();

            // email
            Mail::to($this->_user)->send(new Welcome($this->_user, $password));
            $response = ['success' => true, 'message' => 'Company admin has been registered.'];
        }

        return response()->json($response);
    }


    public function all() {
        return DataTables::of(Role::companyAdmin()->users()->where('company_id', auth()->user()->company_id))
            ->addColumn('actions', function ($user) {
                return '<button class="btn btn-sm btn-warning ad-edit-btn" 
                                source="' . route('get-company-admin', $user->id) . '">'
                    . '<i class="fa fa-edit"></i> Edit</button>&nbsp;'
                    . '<button class="btn btn-sm btn-danger ad-del-btn" 
                                source="' . route('delete-company-admin', $user->id) . '">'
                    . '<i class="fa fa-trash"></i> Delete</button>&nbsp;'
                    . '<form action="'. route('reset-password', $user->id) .'" method="post" style="display: inline;">'
                    . csrf_field()
                    . method_field('PUT')
                    . '<button class="btn btn-sm btn-default" 
                                source="' . route('reset-password', $user->id) . '">'
                    . '<i class="fa fa-lock"></i> Reset Password</button></form>';
            })
            ->rawColumns(['actions'])
            ->make();
    }

    public function destroy($id) {
        try {
            User::destroy($id);
            return response()->json(['success' => true, 'message' => 'Company admin has been deleted.']);
        } catch(QueryException $qe) {
            return response()->json(['success' => false, 'message' => 'Failed to delete the company admin!']);
        }
    }
    private function message($info, $password) {
        $message = "Hi, use the following credentials to login into ". route('login') ."\n"
            . "- Phone Number: " . $info['phone_no'] . "\n"
            . "- Password: " . $password;

        return $message;
    }


    public function show(User $user) {
        $user_data = $user->toArray();
        $user_data['phone_no'] = ltrim($user_data['phone_no'], '254');
        return $user_data;
    }

    public function update() {
        $id = request('id');

        // validation
        $validator = Validator::make(request()->all(), [
            'first_name' => 'required',
            'middle_name' => 'required',
            'surname' => 'required',
            'id_no' => 'required|unique:users,id_no,' . $id,
            'phone_no' => 'required',
            'email' => 'required|unique:users,email,' . $id,
            'id' => 'required|numeric',
            'photo' => 'image|max:5000'
        ],
            [
                'phone_no.unique' => 'The Phone No('. request('phone_no') .') already exists!',
                'id_no.unique' => 'The ID No('. request('id_no') .') already exists!'
            ]
        );
        $info = request(['first_name', 'middle_name', 'surname', 'id_no', 'phone_no', 'email']);
        $phone_no = trim(request('phone_no'), '0');
        $info['phone_no'] = '254' . $phone_no;

        $validator->after(function($validator) use($id, $info) {
            $exists = User::where('phone_no', $info['phone_no'])
                ->where('id', '<>', $id)
                ->count();
            if ($exists) {
                $validator->errors()
                    ->add(
                        'phone_no',
                        'The Phone No('. $info['phone_no'] .') already exist!)'
                    );
            }
        });

        // upload the image
        if (request()->has('photo')) {
            $path = request('photo')->store('public');
            $path_items = explode('/', $path);
            $info['photo'] = json_encode([
                'original_path' => $path,
                'asset_path' => asset('storage/' . end($path_items))
            ]);
        }

        if ($validator->fails()) {
            $response = ['success' => false, 'errors' => $validator->getMessageBag()->toArray()];
            return response()->json($response);
        } else {



            try {
                User::where('id', request('id'))->update($info);
            } catch (QueryException $qe) {
                $this->_errors['company_admin'] = 'Encountered an error while updating  the company admin details; ' . $qe->getMessage();
            }
        }

        if (count($this->_errors) === 0) {
            $response = ['success' => true, 'message' => 'Company has been updated'];
        } else {
            $response = ['errors' => $this->_errors, 'success' => false];
        }

        return response()->json($response);
    }
}

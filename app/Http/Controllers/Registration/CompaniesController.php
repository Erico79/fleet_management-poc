<?php

namespace App\Http\Controllers\Registration;

use App\Company;
use App\Mail\Welcome;
use App\Repositories\Registration;
use App\Repositories\SMS;
use App\Role;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\AfricasTalkingGateway;

class CompaniesController extends Controller
{
    private $_company, $_user,
        $_errors = [];

    public function index() {
        return view('system-admin.registration.companies');
    }

    public function store(SMS $sms) {
        $validator = Validator::make(request()->all(), [
            'name' => 'required|unique:companies',
            'company_no' => 'required|unique:companies',
            'postal_address' => 'required',
            'physical_address' => 'required',
            'telephone_no' => 'required|numeric|unique:companies',
            'email' => 'required|email|unique:users'
        ],
        [
            'name.unique' => 'The Company('. request('name') .') is already registered.',
            'company_no.unique' => 'The Company No('. request('company_no') .') already exists!',
            'telephone_no.unique' => 'The Telephone No('. request('telephone_no') .') already exists!',
            'email.unique' => 'The Email Address('. request('email') .') already exists!',
        ]
        );
        $info = request(['name', 'company_no', 'postal_address', 'physical_address', 'telephone_no', 'email']);
        $phone_no = trim(request('telephone_no'), '0');
        $info['telephone_no'] = '254' . $phone_no;
        $password = Registration::generateStrongPassword();

        $validator->after(function($validator) use($info) {
            $exists = User::where('phone_no', $info['telephone_no'])->count();
            if ($exists) {
                $validator->errors()
                    ->add(
                        'phone_no',
                        'The Phone No('. $info['telephone_no'] .') already exist!)'
                    );
            }
        });

        if (!$validator->fails()) {
            DB::transaction(function () use ($info, $password, $validator) {
                // register company
                try {
                    $this->_company = Company::create($info);
                } catch (QueryException $qe) {
                    $this->_errors['company'] = 'Encountered an error while registering the company.';
                }

                if ($this->_company) {
                    try {
                        // create company admin
                        $this->_company->users()->create([
                            'email' => $info['email'],
                            'phone_no' => $info['telephone_no'],
                            'password' => bcrypt($password),
                            'role_id' => Role::companyAdmin()->id
                        ]);
                    } catch (QueryException $qe) {
                        $this->_errors['company_admin'] = 'Encountered an error while registering the company admin. - ' . $qe->getMessage();
                    }
                }

                $this->_user = User::where('email', $info['email'])->first();
            });
        } else {
            $response = ['success' => false, 'errors' => $validator->getMessageBag()->toArray()];
            return response()->json($response);
        }

        if (count($this->_errors)) {
            $response = ['success' => false, 'errors' => $this->_errors];
        } else {
            // send the Company Admin's credentials to the email/sms
            $info['password'] = $password;
            $sms->setRecipients($info['telephone_no']);
            $sms->setMessage($this->message($info));
            $sms->send();

            // email
            Mail::to($this->_user)->send(new Welcome($this->_user, $password));
            $response = ['success' => true, 'message' => 'Company info has been registered.'];
        }

        return response()->json($response);
    }

    public function all() {
        return DataTables::of(Company::all())
            ->addColumn('actions', function($company) {
                return '<button class="btn btn-sm btn-warning co-edit-btn" 
                            source="' . route('get-company', $company->id). '">'
                    . '<i class="fa fa-edit"></i> Edit</button>&nbsp;'
                    . '<button class="btn btn-sm btn-danger co-del-btn" 
                            source="' . route('delete-company', $company->id). '">'
                    . '<i class="fa fa-trash"></i> Delete</button>';
            })
            ->rawColumns(['actions'])
            ->make();
    }

    public function destroy($id) {
        try {
            Company::destroy($id);
            return response()->json(['success' => true, 'message' => 'Company has been deleted.']);
        } catch(QueryException $qe) {
            return response()->json(['success' => false, 'message' => 'Failed to delete the company!']);
        }
    }

    private function message($info) {
        $message = "Hi, use the following credentials to login into ". route('login') ."\n"
            . "- Phone Number: " . $info['telephone_no'] . "\n"
            . "- Password: " . $info['password'];

        return $message;
    }

    public function show(Company $company) {
        $company_data = $company->toArray();
        $company_data['telephone_no'] = ltrim($company_data['telephone_no'], '254');
        return $company_data;
    }

    public function update() {
        $id = request('id');

        // validation
        $validator = Validator::make(request()->all(), [
            'name' => 'required|unique:companies,name,' . $id,
            'company_no' => 'required|unique:companies,company_no,' . $id,
            'postal_address' => 'required',
            'physical_address' => 'required',
            'telephone_no' => 'required|numeric|unique:companies,telephone_no,' . $id,
            'email' => 'required|email',
            'id' => 'required|numeric'
        ],
            [
                'name.unique' => 'The Company('. request('name') .') is already registered.',
                'company_no.unique' => 'The Company No('. request('company_no') .') already exists!',
                'telephone_no.unique' => 'The Telephone No('. request('telephone_no') .') already exists!',
            ]
        );

        $phone_no = trim(request('telephone_no'), '0');
        $info = request(['name', 'company_no', 'postal_address', 'physical_address', 'telephone_no', 'email', 'id']);
        $info['telephone_no'] = '254' . $phone_no;

        $validator->after(function($validator) use($id, $info) {
            $exists = User::where('phone_no', $info['telephone_no'])
                ->where('company_id', '<>', $id)
                ->count();
            if ($exists) {
                $validator->errors()
                    ->add(
                        'phone_no',
                        'The Phone No('. $info['telephone_no'] .') already exist!)'
                    );
            }

            if (User::where('email', request('email'))->where('company_id', '<>', $id)->count()) {
                $validator->errors()->add('email', 'The Email Address (' . request('email') . ') already exists!');
            }
        });

        if (!$validator->fails()) {

            DB::transaction(function() use($info) {
                try {
                    $this->_company = Company::where('id', $info['id'])->update($info);
                } catch (QueryException $qe) {
                    $this->_errors['company'] = 'Encountered an error while updating the company detailing.';
                }

                try {
                    // create company admin
                    User::where('email', $info['email'])->update([
                        'email' => $info['email'],
                        'phone_no' => $info['telephone_no'],
                        'role_id' => Role::companyAdmin()->id
                    ]);
                } catch (QueryException $qe) {
                    $this->_errors['company_admin'] = 'Encountered an error while updating  the company admin details';
                }
            });
        } else {
            $response = ['success' => false, 'errors' => $validator->getMessageBag()->toArray()];
            return response()->json($response);
        }

        if (count($this->_errors) == 0) {
            $response = ['success' => true, 'message' => 'Company info has been updated.'];
        } else {
            $response = ['errors' => $this->_errors, 'success' => false];
        }

        return response()->json($response);
    }

    public function sendSMS(SMS $sms) {
        $sms->setMessage('Hi Eric, Nice to see you.');
        $sms->setRecipients('254718513948');
        $sms->send();
    }
}

<?php

namespace App\Http\Controllers;

use App\Repositories\Company;
use App\Route;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($user = Company::authCompanyAdmin()) {
            $routes = $user->company->routes;

            $code = strtolower(auth()->user()->role->code);
            return view("{$code}/home", [
                'routes' => $routes
            ]);
        } else {
            return view('home');
        }
    }

    public function loadRoute(Route $route) {
        return $route;
    }
}

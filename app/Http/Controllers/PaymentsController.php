<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Role;
use Yajra\DataTables\DataTables;

class PaymentsController extends Controller
{
    public function index() {
        $owner_vehicles = auth()->user()->masterfile->vehicles()->pluck('registration_no')->toArray();
        $payments = Payment::whereIn('registration_no', $owner_vehicles)->get();
        return view('payments.index', ['payments' => $payments]);
    }

    public function routePayments($route_id) {
        $date_range = request(['from_date', 'to_date']);
        $payments = Payment::getPayments($route_id, $date_range);

        if (request()->has('reg_no') && !empty(request('reg_no')))
            $payments = Payment::getPayments($route_id, $date_range, request('reg_no'));

        return DataTables::of($payments)
            ->editColumn('vehicle_total', function($payment) {
                return '<strong>Ksh. ' . $payment->vehicle_total . '</strong>';
            })
            ->rawColumns(['vehicle_total'])
            ->make();
    }

    public function chartData($route_id) {
        $date_range = request(['from_date', 'to_date']);
        $payments = Payment::getPayments($route_id, $date_range);

        if (request()->has('reg_no') && !empty(request('reg_no')))
            $payments = Payment::getPayments($route_id, $date_range, request('reg_no'));

        $response['datasets'] = [
            'label' => 'Amount Paid',
            'backgroundColor' => 'rgba(114, 227, 152, 0.8)',
            'borderColor' => 'rgba(220, 220, 20, 0.8)',
            'highlightFill' => 'rgba(220, 220, 20, 0.75)',
            'highlightStroke' => 'rgba(220, 22, 220, 1)'
        ];

        if (count($payments)) {
            foreach ($payments as $payment) {
                $response['labels'][] = $payment->registration_no;
                $response['datasets']['data'][] = $payment->vehicle_total;
            }
        }

        $response['datasets'] = [$response['datasets']];
        return response()->json($response);
    }

    public function driverPayments() {
        return view('driver.payments');
    }

    public function driverPaymentsDT() {
        $reg_nos = auth()->user()->driver->vehicles()->pluck('registration_no')->toArray();
        $driver_payments = Payment::whereIn('registration_no', $reg_nos)->get();
        return DataTables::of($driver_payments)->make();
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;

class CheckAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            $user = $request->user();
            $curr_route = Route::current();

            $access = $user->role->systemRoutes()
                ->where('url', $curr_route->uri)
                ->count();
            if ($access)
                return $next($request);
            else
                abort(403, 'You have been denied access to this page. Please contact the System Admin!');
        }
    }
}

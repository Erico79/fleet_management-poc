<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessRole extends Model
{
    protected $guarded = ['id'];
    const Driver = 'DRIVER';
    const Owner = 'Owner';

    public function masterfiles() {
        return $this->hasMany(Masterfile::class);
    }

    public static function driver() {
        return self::where('code', self::Driver)->first();
    }

    public static function owner() {
        return self::where('code', self::Owner)->first();
    }
}

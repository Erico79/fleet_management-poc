<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guarded = ['id'];

    const Driver = 'Driver';
    const CompanyAdmin = 'CompanyAdmin';
    const SystemAdmin = 'SystemAdmin';

    public static function driver() {
        return self::where('code', self::Driver)->first();
    }

    public static function companyAdmin() {
        return self::where('code', self::CompanyAdmin)->first();
    }

    public static function systemAdmin() {
        return self::where('code', self::SystemAdmin)->first();
    }

    public function users() {
        return $this->hasMany(User::class);
    }

    public function systemRoutes() {
        return $this->belongsToMany(SystemRoute::class, 'role_route', 'role_id', 'system_route_id');
    }
}

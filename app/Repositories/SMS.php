<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 10/6/18
 * Time: 3:22 PM
 */
namespace App\Repositories;

use Illuminate\Support\Facades\Log;

require_once('AfricasTalkingGateway.php');

class SMS
{
    private $username;
    private $apikey;
    private $sandbox;
    private $recipients;
    private $message;

    public function __construct()
    {
        $this->username = env('SMS_USERNAME');
        $this->apikey = env('SMS_API_KEY');
        $this->sandbox = env('SMS_SANDBOX');
    }

    public function setRecipients($recipients) {
        if (is_array($recipients)) {
            $recs = '';
            if (count($recipients)) {
                foreach ($recipients as $recipient) {
                    $recs += '+'. $recipients . ',';
                }
            }

            $this->recipients = rtrim($recs, ',');
        } else {
            $this->recipients = $recipients;
        }
    }

    public function getRecipients() {
        return $this->recipients;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function send() {
        // Specify the numbers that you want to send to in a comma-separated list
        // Please ensure you include the country code (+254 for Kenya in this case)
        $recipients = $this->recipients;

        // And of course we want our recipients to know what we really do
        $message    = $this->message;

        // Create a new instance of our awesome gateway class
        $gateway    = new AfricasTalkingGateway($this->username, $this->apikey);
        /*************************************************************************************
        NOTE: If connecting to the sandbox:
        1. Use "sandbox" as the username
        2. Use the apiKey generated from your sandbox application
        https://account.africastalking.com/apps/sandbox/settings/key
        3. Add the "sandbox" flag to the constructor
        $gateway  = new AfricasTalkingGateway($username, $apiKey, "sandbox");
         **************************************************************************************/
        // Any gateway error will be captured by our custom Exception class below,
        // so wrap the call in a try-catch block
        try
        {
            // Thats it, hit send and we'll take care of the rest.
            $results = $gateway->sendMessage($recipients, $message);

//            foreach($results as $result) {
//                // status is either "Success" or "error message"
//                echo " Number: " .$result->number;
//                echo " Status: " .$result->status;
//                echo " StatusCode: " .$result->statusCode;
//                echo " MessageId: " .$result->messageId;
//                echo " Cost: "   .$result->cost."\n";
//            }

            Log::info('SMS: ' . json_encode($results));
        }
        catch ( AfricasTalkingGatewayException $e )
        {
//            echo "Encountered an error while sending: ".$e->getMessage();
            Log::info('SMS: ' . json_encode($e->getMessage));
        }
    }
}
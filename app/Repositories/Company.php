<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 10/10/18
 * Time: 4:08 PM
 */

namespace App\Repositories;


use App\Role;

class Company
{
    public static function authCompanyAdmin() {
        if (auth()->user()->role->id == Role::companyAdmin()->id)
            return auth()->user();

        return null;
    }
}
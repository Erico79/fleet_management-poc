<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemRoute extends Model
{
    protected $guarded = ['id'];

    public function roles() {
        return $this->belongsToMany(Role::class, 'role_route', 'system_route_id', 'role_id');
    }
}

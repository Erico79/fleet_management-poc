<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Payment extends Model
{
    protected $guarded = ['id'];

    public static function getPayments($route_id, $date_range, $reg_no = null) {
        $query =  DB::table('payments')
            ->leftJoin('vehicles', 'payments.registration_no', '=', 'vehicles.registration_no')
            ->select(DB::raw('sum(amount) as vehicle_total'), 'payments.registration_no')
            ->groupBy('payments.registration_no')
            ->where('vehicles.route_id', $route_id)
            ->whereBetween('payments.created_at', [$date_range['from_date'], $date_range['to_date']]);

        if ($reg_no)
            $query->where('payments.registration_no', $reg_no);

        return $query->get();
    }
}

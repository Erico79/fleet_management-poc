<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $guarded = ['id'];

    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function createdBy() {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
    public function driver() {
        return $this->belongsTo(Driver::class);
    }

    public function route() {
        return $this->belongsTo(Route::class);
    }
}

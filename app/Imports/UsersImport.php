<?php

namespace App\Imports;

use App\Role;
use App\User;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToModel
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
            'first_name' => $row[1],
            'middle_name' => $row[2],
            'surname' => $row[3],
            'email' => $row[4],
            'phone_no' => $row[5],
            'id_no' => $row[6],
            'password' => bcrypt('pass123'),
            'role_id' => Role::driver()->id,
            'company_id' => 1,
        ]);
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('layouts.partials.sidebar', function($view) {
            $menu = config('menu');
            $menu = $menu[strtolower(auth()->user()->role->code)];
            $view->with(['menu' => $menu]);
        });

        view()->composer('layouts.partials.header', function($view) {
            $user = auth()->user();
            $photo_data = $user->photo ? json_decode($user->photo) : [];
            $view->with([
                'photo' => isset($photo_data->asset_path) ? $photo_data->asset_path : 'images/avatar.svg'
            ]);
        });
    }
}

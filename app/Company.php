<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded = ['id'];

    public function users() {
        return $this->hasMany(User::class);
    }

    public function vehicles() {
        return $this->hasMany(Vehicle::class);
    }

    public function routes() {
        return $this->hasMany(Route::class);
    }
    public function drivers() {
        return $this->hasMany(Driver::class);
    }

}

@extends('layouts.datatable')
@section('title', 'Vehicles')

@push('css')
    <style>
        .table-data2.table tbody td {
            padding: 10px 10px !important;
            font-size: small;
            text-align: center;
        }
    </style>
@endpush

@section('filters')

@endsection

@section('buttons')
    <button class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#vehicles-modal">
        <i class="zmdi zmdi-plus"></i>Add a Vehicle</button>
@endsection

@section('modals')
    {{--setup the routes modal as shown below--}}
    <div class="modal fade" id="vehicles-modal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Add a New Vehicle</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('vehicle') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="registration_no" class="control-label mb-1">Registration No</label>
                            <input id="registration_no" name="registration_no" type="text" class="form-control" aria-required="true" aria-invalid="false" >
                        </div>
                            <div class="form-group">
                                <label for="capacity" class="control-label mb-1">Vehicle Capacity</label>
                                <input id="capacity" name="capacity" type="text" class="form-control" aria-required="true" aria-invalid="false" >
                            </div>
                        <div class="form-group mb-4">
                            <label class="control-label bmd-label-static" for="route_id">Route</label>
                            <select class="form-control mt-2 pb-0" name="route_id" id="route_id" required>
                                <option value=""></option>
                                @if(count( $routes))
                                    @foreach($routes as $route)
                                        <option value="{{ $route->id }}">{{ $route->from.' -  '.$route->to}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group mb-4">
                            <label class="control-label bmd-label-static" for="driver_id">Driver</label>
                            <select class="form-control mt-2 pb-0" name="driver_id" id="driver_id" required>
                                <option value=""></option>
                                @if(count( $drivers))
                                    @foreach($drivers as $driver)
                                        <option value="{{ $driver->driver->id }}">{{ $driver->name.' -  '.$driver->driver->license_ref_no }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group mb-4">
                            <label class="control-label mb-1" for="status">Status</label>
                            <select class="form-control mt-2 pb-0" name="active" id="active" required>
                                <option value="0">Active</option>
                                <option value="1">Inactive</option>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('datatable')
    <div class="card-body">
        @include('layouts.common.success')
        @include('layouts.common.warnings')
        @include('layouts.common.warning')
    </div>
    <table class="table table-data2 table-bordered table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Registration No</th>
            <th>Capacity</th>
            <th>Route</th>
            <th>Driver</th>
            <th>status</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        {{--your foreach loop should enclose the following code--}}
        @if(count($vehicles))
            @foreach($vehicles as $vehicle)
            <td>{{$vehicle->id}}</td>
            <td>{{$vehicle->registration_no}}</td>
            <td>{{$vehicle->route->from.' '.$vehicle->route->to}}</td>
            <td class="text-success font-weight-bold">{{$vehicle->driver->masterfile->name}}</td>
            <td>{{$vehicle->active ? 'Inactive' : 'Active'}}</td>
            <td>
                <div class="table-data-feature">
                    <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                        <i class="zmdi zmdi-edit"></i>
                    </button>
                    <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                        <i class="zmdi zmdi-delete"></i>
                    </button>
                </div>
            </td>
        </tr>
        <tr class="spacer"></tr>
        {{--endforeach--}}
            @endforeach
        @endif
        </tbody>
    </table>
@endsection
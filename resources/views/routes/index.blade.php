@extends('layouts.datatable')
@section('title', 'Routes')

@push('css')
    <style>
        .table-data2.table tbody td {
            padding: 10px 10px !important;
            font-size: small;
        }
    </style>
@endpush

@section('filters')

@endsection

@section('buttons')
    <button class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#routes-modal">
        <i class="zmdi zmdi-plus"></i>Add Route</button>
@endsection
@section('modals')
    {{--setup the routes modal as shown below--}}
    <div class="modal fade" id="routes-modal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Create a New Route</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('route') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="from" class="control-label mb-1">From</label>
                            <input id="from" name="from" type="text" class="form-control" aria-required="true" aria-invalid="false" >
                        </div>
                        <div class="form-group has-success">
                            <label for="to" class="control-label mb-1">To</label>
                            <input id="to" name="to" type="text" class="form-control cc-name valid" data-val="true" aria-required="true" aria-invalid="false" aria-describedby="cc-name-error">

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('datatable')
    <div class="card-body">
        @include('layouts.common.success')
        @include('layouts.common.warnings')
        @include('layouts.common.warning')
        <div id="successView" class="alert alert-success" style="display:none;">
            <button class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong><span id="successData"></span>
        </div>
    </div>
    <table class="table table-data2 table-bordered table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>From</th>
            <th>To</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        {{--your foreach loop should enclose the following code--}}
        @if(count($routes))
            @foreach($routes as $route)
                <tr class="tr-shadow">
            <td>{{$route->id}}</td>
            <td>{{$route->from}}</td>
            <td>{{$route->to}}</td>
            <td>
                <div class="table-data-feature">
                    <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                        <i class="zmdi zmdi-edit"></i>
                    </button>
                    <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                        <i class="zmdi zmdi-delete"></i>
                    </button>
                </div>
            </td>
        </tr>
        <tr class="spacer"></tr>
        @endforeach
            @endif
        </tbody>
    </table>
@endsection
@extends('layouts.datatable')
@section('title', 'Drivers')

@push('css')
    <style>
        .table-data2.table tbody td {
            padding: 10px 10px !important;
            font-size: small;
            text-align: center;
        }
    </style>
@endpush

@section('filters')

@endsection

@section('datatable')
    <div class="card-body mt-0 p-0">
        @include('layouts.common.success')
        @include('layouts.common.warnings')
        @include('layouts.common.warning')
    </div>
    <table class="table table-data2 table-bordered table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Driver's Name</th>
            <th>License Ref#</th>
            <th>National ID#</th>
            <th>Phone No</th>
        </tr>
        </thead>
        <tbody>
        {{--your foreach loop should enclose the following code--}}
        @if(count($drivers))
            @foreach($drivers as $driver)
                <tr>
                    <td>{{$driver->id}}</td>
                    <td class="font-weight-bold">{{$driver->name}}</td>
                    <td>{{$driver->driver->license_ref_no}}</td>
                    <td>{{$driver->id_no}}</td>
                    <td>{{$driver->phone_no}}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endsection
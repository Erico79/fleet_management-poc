@component('mail::message')
# Welcome to {{ config('app.name') }}

Dear user,
Use the following credentials to login to the system;

<ul>
    <li>You can either use your Email <b>{{ $user->email }}</b> or Phone Number <b>{{ $user->phone_no }}</b></li>
    <li>Your password is: <code><b>{{ $password }}</b></code></li>
</ul>

Thanks,<br>
{{ config('app.name') }}
@endcomponent

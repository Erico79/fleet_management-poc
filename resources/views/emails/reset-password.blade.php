@component('mail::message')
# Password Reset

Dear {{ $user->first_name }}, your password has been reset. Your login credentials are as follows;
<ul>
    <li>Email: {{ $user->email }}</li>
    <li>Password: <code>{{ $password }}</code></li>
</ul>

@component('mail::button', ['url' => url('login')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

@extends('layouts.page')
@section('title', 'Login')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-group">
                <div class="card p-4">
                    <div class="card-body">
                        <h1>Login</h1>
                        <p class="text-muted">Sign In to your account</p>
                        <form action="{{ route('login') }}" method="post">
                            @csrf
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="icon-user"></i>
                                    </span>
                                </div>
                                <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                       type="text" name="email" placeholder="Email/Phone Number"
                                       value="{{ old('email') }}">
                                <div class="valid-feedback text-dark font-weight-bold" style="display: block">Enter your Email/Phone Number</div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="icon-lock"></i>
                                    </span>
                                </div>
                                <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password" type="password" placeholder="Password"
                                       value="{{ old('password') }}">
                                <span class="valid-feedback text-dark font-weight-bold" style="display: block">Enter your Password</span>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="row">
                            <div class="col-6">
                                <button class="btn btn-primary px-4">Login</button>
                            </div>
                            {{--<div class="col-6 text-right">--}}
                                {{--<button class="btn btn-link px-0" type="button">Forgot password?</button>--}}
                            {{--</div>--}}
                        </div>
                        </form>
                    </div>
                </div>
                <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
                    <div class="card-body text-center">
                        <div>
                            <h2>{{ config('app.name') }}</h2>
                            <p>{{ config('app.name') }} is an analytical app for fleet companies.
                            This app allows fleet owners to keep track of mobile payments done by customers,
                            for goods transportation services.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

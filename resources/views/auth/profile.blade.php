@extends('layouts.child-layouts.datatables')
@section('title', 'My Profile')
@push('js')
    <script src="{{ asset('js/registration/profile.js') }}"></script>
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item active">My Profile</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header pt-2">
            <strong>{{ $user->first_name . ' ' . $user->middle_name . ' ' . $user->surname }}</strong>
        </div>
        <div class="card-body">
            <div class="mb-3">
                <button source="{{ route('edit-profile', $user->id) }}"
                        class="btn btn-outline-primary " id="edit-profile-btn" pr-id="{{ $user->id }}"
                        profile-url="{{ route('my-profile') }}">
                    <i class="fa fa-edit"></i> Edit Profile</button>
            </div>
            @include('layouts.common.success')
            @include('layouts.common.warnings')
            @include('layouts.common.warning')
            <div class="row">
                <div class="col-md-3">
                    <div>
                        <div class="circle">
                            <!-- User Profile Image -->
                            <img class="profile-pic" src="{{ $photo }}">

                            <!-- Default Image -->
                            <!-- <i class="fa fa-user fa-5x"></i> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <ul class="list-group">
                        <li class="list-group-item">Email: <span class="text-muted">{{ $user->email }}</span></li>
                        <li class="list-group-item">Phone: <span class="text-muted">+{{ $user->phone_no }}</span></li>
                        <li class="list-group-item">National ID: <span class="text-muted">{{ $user->id_no }}</span></li>
                        <li class="list-group-item">User Role: <span class="text-muted">{{ $user->role->name }}</span></li>
                        @if($user->company)
                            <li class="list-group-item">Company: <span class="text-muted">{{ $user->company->name }}</span></li>
                        @endif
                        @if($user->role->id === \App\Role::driver()->id)
                            <li class="list-group-item">Membership No: <span class="text-muted">{{ $user->driver->membership_no }}</span></li>
                        @endif
                    </ul>
                </div>
        </div>
    </div>
@endsection

@section('modals')
<div class="modal fade" id="profile-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold font-3xl" id="exampleModalLabel">
                    <span id="profile-modal-title">Edit</span> Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ url('profile') }}" default-action="{{ url('profile') }}" method="post" id="profile-form" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="_method" value="PUT" id="profile-spoof-input" disabled/>
                <div class="modal-body">
                    <div class="row mb-3">
                        <div class="m-auto">
                            <div class="circle">
                                <!-- User Profile Image -->
                                <img class="profile-pic" src="{{ asset('images/avatar.svg') }}"
                                     default-src="{{ asset('images/avatar.svg') }}">

                                <!-- Default Image -->
                                <!-- <i class="fa fa-user fa-5x"></i> -->
                            </div>
                            <div class="p-image">
                                <i class="fa fa-camera upload-button"></i>
                                <input name="photo" class="file-upload" type="file" accept="image/*"/>
                            </div>
                        </div>
                    </div>
                {{--<strong class="font-2xl">Company Info</strong>--}}
                <!-- /.row-->
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="first_name" class="font-weight-bold">First Name</label>
                                <input class="form-control font-weight-bold text-dark" id="first_name" type="text" name="first_name"
                                       placeholder="Enter the First Name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="middle_name" class="font-weight-bold">Middle Name</label>
                                <input class="form-control font-weight-bold text-dark" id="middle_name" type="text" name="middle_name"
                                       placeholder="Enter the Middle Name">
                            </div>
                        </div>

                    </div>
                    <!-- /.row-->
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="surname" class="font-weight-bold">Surname</label>
                                <input class="form-control font-weight-bold text-dark" id="surname" type="text" name="surname"
                                       placeholder="Enter the Surname" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="physical-address" class="font-weight-bold">Email Address</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                    </div>
                                    <input class="form-control font-weight-bold text-dark" id="email" type="email" name="email"
                                           placeholder="Email Address" required>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.row-->
                {{--<strong class="font-2xl">Company Admin</strong>--}}
                <!-- /.row-->
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="phone_no" class="font-weight-bold">Phone Number</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text font-weight-bold">
                                            +254
                                        </span>
                                    </div>
                                    <input class="form-control font-weight-bold text-dark" id="phone_no" type="text" name="phone_no"
                                           placeholder="Phone Number" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.row-->
                    <!-- /.row-->
                    {{--<strong class="font-2xl">Company Admin</strong>--}}
                </div>
                <input type="hidden" class="form-control" name="id" id="id"/>
                <div class="modal-footer">
                    <button class="btn btn-success">
                        <i class="fa fa-dot-circle-o"></i> Submit</button>
                    <button class="btn btn-danger" type="reset">
                        <i class="fa fa-ban"></i> Reset</button>
                    <button class="btn btn-default" type="button" data-dismiss="modal">
                        <i class="fa fa-close"></i> Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
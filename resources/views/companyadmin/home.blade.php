@extends('layouts.child-layouts.datatables')
@section('title', 'Dashboard')

@section('breadcrumb')
    <li class="breadcrumb-item active">Dashboard</li>
@endsection

@push('css')
    <style>
        .card:hover {
            box-shadow: 10px 10px 10px #ccc;
        }

        .route-title * {
            font-size: small;
            text-align: left !important;
        }

        .route-title {
            cursor: pointer;
        }

        .input-group {
            width: 50% !important;
            float: right !important;
            margin-right: 0px;
        }

        @media screen and (max-width: 760px) {
            .input-group {
                margin-top: 0px !important;
                width: 100% !important;
            }
        }

        .progress-xs {
            height: 8px;
        }

        .progress-group-section {
            border: 1px solid #eee;
            padding: 20px 15px;
        }

        .progress-group-section:hover {
            box-shadow: 10px 10px 10px #eee;
        }

        .progress-group-section > .progress-header {
            cursor: pointer;
        }
    </style>
@endpush

@push('js')
    {{--<script src="{{ asset('vendors/chart.js/js/Chart.min.js') }}"></script>--}}
    <script src="{{ asset('js/dashboard.js') }}"></script>
@endpush

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            {{--widgets--}}

            <div class="row">
                <div class="col-md-12 mb-2">
                    <div id="reportrange" class="pull-right"
                         style="background: #08c; cursor: pointer; color: #fff; padding: 5px 10px; border: 1px solid #ccc;">
                        <i class="fa fa-calendar"></i>&nbsp;
                        <span></span> <i class="fa fa-caret-down"></i>
                    </div>
                </div>
            </div>

            @if (count($routes))
                @foreach($routes as $route)
                    <!-- card -->
                    <div class="card" route-id="{{ $route->id }}" source="{{ route('load-route', $route->id) }}">
                        <div class="card-header route-card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <span class="route-title" data-toggle="collapse" href="#route-panel{{ $route->id }}">
                                        <i class="fa fa-road"></i> ROUTE <br/>
                                        From: <strong>{{ $route->from }}</strong><br/>
                                        To: <strong>{{ $route->to }}</strong>
                                    </span>
                                </div>
                                <div class="col-md-6">
                                    <form class="search-reg-form" method="get">
                                        <div class="input-group mt-4" data-toggle="tooltip" data-placement="top"
                                             title="Filter by Vehicle Registration No" style="display: none; border: 1px solid #eee;">
                                                <input class="form-control search-reg-no" style="font-size: small"
                                                       type="text" placeholder="Registration No"/>
                                                <span class="input-group-append">
                                                    <button class="btn btn-default btn-outline">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </span>
                                        </div>
                                    </form>
                                </div>
                            </div>


                        </div>
                        <div class="loader" style="display: none;">Loading...</div>
                        <div class="card-body collapse route-panel" id="route-panel{{ $route->id }}">
                            <div class="row">
                                <div class="col-sm-12 vehicle-section"
                                     vehicles-source="{{ route('load-vehicles', $route->id) }}"
                                ></div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-->
                @endforeach
            @endif
        </div>
    </div>
@endsection

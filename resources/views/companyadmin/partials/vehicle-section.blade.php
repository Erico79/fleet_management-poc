@if(count($route_vehicles))
    <div class="row">
        <div class="form-group col-md-2">
            <select name="no_of_records" class="form-control no_of_records">
                @foreach($rows as $key => $row)
                    <option value="{{ $key }}" {{ $key == $no_of_records ? 'selected' : '' }}>{{ $row }}</option>
                @endforeach
            </select>
        </div>
    </div>

    @foreach($route_vehicles as $vehicle)
        @php
            $query = \App\Payment::where('registration_no', $vehicle->registration_no)
                ->whereBetween('created_at', [$from_date, $to_date]);

            if($reg_no)
                $query->where('registration_no', 'like', '%' . $reg_no . '%');

            $vehicle_total = $query->sum('amount');
            $percentage = 0;
            if ($route_total)
                $percentage = ($vehicle_total / $route_total) * 100;
        @endphp
        <div class="progress-group progress-group-section">
            <div class="progress-header col-12 vehicle-section-div"
                 vehicle-id="{{ $vehicle->id }}"
                 target="#payment-section{{ $vehicle->id }}"
            >
                <div class="progress-group-header align-items-end">
                    <img src="{{ asset('images/truck.svg') }}" style="width: 30px; height: 25px;" class="mr-3"/>
                    <div class="font-weight-bold reg-no-div">{{ $vehicle->registration_no }}</div>
                    <div class="ml-auto font-weight-bold mr-2">Ksh. {{ number_format($vehicle_total, 2) }}</div>
                    <div class="text-muted small">({{ round($percentage, 1) }}%)</div>
                </div>
                <div class="progress-group-bars">
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-success" role="progressbar"
                             style="width: {{ round($percentage) }}%" aria-valuenow="{{ round($percentage) }}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            <div class="table-responsive mt-3" style="display: none" id="payment-section{{ $vehicle->id }}">
                <table id="vehicle-dt-{{ $vehicle->id }}" style="width: 100%"
                   class="table table-responsive-sm table-hover table-outline mb-0"
                       source="{{ route('vehicle-payments-dt', $vehicle->id) }}"
                >
                    <thead class="thead-light">
                    <tr>
                        <th></th>
                        <th>Driver</th>
                        <th>Vehicle Registration No</th>
                        <th>Amount</th>
                        <th>Customer Phone No</th>
                        <th>Transaction Time</th>
                        <th>Transaction ID</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    @endforeach
    {{ $no_of_records != 'all' ? $route_vehicles->links() : '' }}
@endif
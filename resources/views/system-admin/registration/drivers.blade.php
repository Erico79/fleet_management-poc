@extends('layouts.child-layouts.datatables')
@section('title', 'Register a Driver')


@push('my-js')
    <script src="{{ asset('js/registration/drivers.js') }}"></script>
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item active">Manage Drivers</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header pt-2">
            <strong>Manage Drivers</strong>
        </div>
        <div class="card-body">
            <div class="mb-3">
                <button class="btn btn-sm btn-primary" id="add-driver-btn" data-toggle="modal" data-target="#driver-modal">
                    <i class="fa fa-plus"></i> Register Driver</button>
                <a href="{{ route('drivers-import-template') }}" class="btn btn-sm btn-outline-info">
                    <i class="fa fa-download"></i> Download Excel Template</a>
                <button class="btn btn-sm btn-default" id="import-drivers-btn" data-toggle="modal" data-target="#import-modal">
                    <i class="fa fa-file-excel-o"></i> Import Drivers from Excel File</button>
            </div>
            @include('layouts.common.success')
            @include('layouts.common.warnings')
            @include('layouts.common.warning')
            @include('layouts.common.info')
            <table id="driver-dt" source="{{ route('driver-dt') }}"
                   class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Names</th>
                        <th>Membership No</th>
                        <th>National ID#</th>
                        <th>Phone No</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="driver-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold font-3xl" id="exampleModalLabel">
                        <span id="driver-modal-title">Register</span> Driver</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <form action="{{ url('driver') }}" default-action="{{ url('driver') }}" method="post" id="driver-form" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="_method" value="PUT" id="driver-spoof-input" disabled/>
                        <div class="modal-body">
                            <div class="row mb-3">
                                <div class="m-auto">
                                    <div class="circle">
                                        <!-- User Profile Image -->
                                        <img class="profile-pic" src="{{ asset('images/avatar.svg') }}"
                                             default-src="{{ asset('images/avatar.svg') }}">

                                        <!-- Default Image -->
                                        <!-- <i class="fa fa-user fa-5x"></i> -->
                                    </div>
                                    <div class="p-image">
                                        <i class="fa fa-camera upload-button"></i>
                                        <input name="photo" class="file-upload" type="file" accept="image/*"/>
                                    </div>
                                </div>
                            </div>
                            {{--<strong class="font-2xl">Company Info</strong>--}}
                            <!-- /.row-->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="first_name" class="font-weight-bold">First Name</label>
                                            <input class="form-control font-weight-bold text-dark" id="first_name" type="text" name="first_name"
                                                   placeholder="Enter the First Name" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="middle_name" class="font-weight-bold">Middle Name</label>
                                            <input class="form-control font-weight-bold text-dark" id="middle_name" type="text" name="middle_name"
                                                   placeholder="Enter the Middle Name" required>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.row-->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="surname" class="font-weight-bold">Surname</label>
                                            <input class="form-control font-weight-bold text-dark" id="surname" type="text" name="surname"
                                                   placeholder="Enter the Surname" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="physical-address" class="font-weight-bold">Email Address</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-envelope"></i>
                                                    </span>
                                                </div>
                                                <input class="form-control font-weight-bold text-dark" id="email" type="email" name="email"
                                                       placeholder="Email Address">
                                                <div class="valid-feedback text-dark font-weight-bold" style="display: block">
                                                    Driver's Email Address</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.row-->
                            {{--<strong class="font-2xl">Company Admin</strong>--}}
                            <!-- /.row-->
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="phone_no" class="font-weight-bold">Phone Number</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text font-weight-bold">
                                                        +254
                                                    </span>
                                                </div>
                                                <input class="form-control font-weight-bold text-dark" id="phone_no" type="text" name="phone_no"
                                                       placeholder="Phone Number" required>
                                                <div class="valid-feedback font-weight-bold text-dark" style="display: block">
                                                    Driver's Phone Number</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="id_no" class="font-weight-bold">ID NO</label>
                                            <input class="form-control font-weight-bold text-dark" id="id_no" type="text" name="id_no"
                                                   placeholder="Enter the ID NO">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="membership_no" class="font-weight-bold">Membership NO</label>
                                            <input class="form-control font-weight-bold text-dark" id="membership_no" type="text" name="membership_no"
                                                   placeholder="Enter the Membership NO">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row-->
                            <!-- /.row-->
                            {{--<strong class="font-2xl">Company Admin</strong>--}}
                        </div>
                        <input type="hidden" class="form-control" name="id" id="id"/>
                        <div class="modal-footer">
                            <button class="btn btn-success">
                                <i class="fa fa-dot-circle-o"></i> Submit</button>
                            <button class="btn btn-danger" type="reset">
                                <i class="fa fa-ban"></i> Reset</button>
                            <button class="btn btn-default" type="button" data-dismiss="modal">
                                <i class="fa fa-close"></i> Close</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="import-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold font-3xl" id="exampleModalLabel">
                        <span id="driver-modal-title">Import</span> Drivers from Excel File</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('import-drivers') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="drivers-import" class="font-weight-bold">Upload Excel File</label>
                                    <input type="file" accept=".xls,.xlsx" class="form-control" type="text" name="drivers_import" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" name="id" id="id"/>
                    <div class="modal-footer">
                        <button class="btn btn-success">
                            <i class="fa fa-dot-circle-o"></i> Submit</button>
                        <button class="btn btn-danger" type="reset">
                            <i class="fa fa-ban"></i> Reset</button>
                        <button class="btn btn-default" type="button" data-dismiss="modal">
                            <i class="fa fa-close"></i> Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
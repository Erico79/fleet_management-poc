@extends('layouts.child-layouts.datatables')
@section('title', 'Register a Company')

@push('my-js')
    <script src="{{ asset('js/registration/companies.js') }}"></script>
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item active">Manage Companies</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header pt-2">
            <strong>Manage Companies(Fleet Owners)</strong>
        </div>
        <div class="card-body">
            <div class="mb-3">
                <button class="btn btn-sm btn-primary" id="add-btn" data-toggle="modal" data-target="#the-modal">
                    <i class="fa fa-plus"></i> Register Company</button>
            </div>
            <table id="companies-dt" source="{{ route('companies-dt') }}"
                   class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Company#</th>
                        <th>Physical Address</th>
                        <th>Postal Address</th>
                        <th>Phone No</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="the-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold font-3xl" id="exampleModalLabel">
                        <span id="form-title">Register</span> Company</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <form action="{{ url('company') }}" method="post" id="company-form">
                        @csrf
                        <input type="hidden" name="_method" value="PUT" id="spoof-input" disabled/>
                        <div class="modal-body">
                            {{--<strong class="font-2xl">Company Info</strong>--}}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="company-name" class="font-weight-bold">Company Name</label>
                                        <input class="form-control font-weight-bold text-dark" id="company-name" type="text" name="name"
                                               placeholder="Enter the Company's Name" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="company-number" class="font-weight-bold">Company Number</label>
                                        <input class="form-control font-weight-bold text-dark" id="company-number" type="text" name="company_no"
                                               placeholder="Enter the Company's Number" required>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row-->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="postal-address" class="font-weight-bold">Postal Address</label>
                                        <input class="form-control font-weight-bold text-dark" id="postal-address" type="text" name="postal_address"
                                               placeholder="Example, 12345 - 00100, Nairobi" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="physical-address" class="font-weight-bold">Physical Address</label>
                                        <input class="form-control font-weight-bold text-dark" id="physical-address" type="text" name="physical_address"
                                               placeholder="Enter the Physical Address">
                                    </div>
                                </div>
                            </div>
                            <!-- /.row-->
                            {{--<strong class="font-2xl">Company Admin</strong>--}}
                            <!-- /.row-->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="tel-no" class="font-weight-bold">Phone Number</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                    <span class="input-group-text font-weight-bold">
                                                        +254
                                                    </span>
                                            </div>
                                            <input class="form-control font-weight-bold text-dark" id="tel-no" type="text" name="telephone_no"
                                                   placeholder="Phone Number" required>
                                            <div class="valid-feedback font-weight-bold text-dark" style="display: block">
                                                Company Administrator's Phone Number</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="physical-address" class="font-weight-bold">Email Address</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-envelope"></i>
                                                    </span>
                                            </div>
                                            <input class="form-control font-weight-bold text-dark" id="email" type="email" name="email"
                                                   placeholder="Email Address" required>
                                            <div class="valid-feedback text-dark font-weight-bold" style="display: block">
                                                Company Administrator's Email Address</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row-->
                        </div>
                        <input type="hidden" name="id"/>
                        <div class="modal-footer">
                            <button class="btn btn-success">
                                <i class="fa fa-dot-circle-o"></i> Submit</button>
                            <button class="btn btn-danger" type="reset">
                                <i class="fa fa-ban"></i> Reset</button>
                            <button class="btn btn-default" type="button" data-dismiss="modal">
                                <i class="fa fa-close"></i> Close</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.child-layouts.datatables')
@section('title', 'Register a Vehicle')


@push('my-js')
    <script src="{{ asset('js/vehicles.js') }}"></script>
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item active">Manage Vehicles</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header pt-2">
            <strong>Manage Vehicles</strong>
        </div>
        <div class="card-body">
            <div class="mb-3">
                <button class="btn btn-sm btn-primary" id="add-vehicle-btn" data-toggle="modal" data-target="#vehicle-modal">
                    <i class="fa fa-plus"></i> Register Vehicle</button>
                <a href="{{ route('vehicles-import-template') }}" class="btn btn-sm btn-outline-info">
                    <i class="fa fa-download"></i> Download Excel Template</a>
                <button class="btn btn-sm btn-default" id="import-vehicles-btn" data-toggle="modal" data-target="#import-modal">
                    <i class="fa fa-file-excel-o"></i> Import Vehicles from Excel File</button>
            </div>
            @include('layouts.common.success')
            @include('layouts.common.warnings')
            @include('layouts.common.warning')
            @include('layouts.common.info')
            <table id="vehicle-dt" source="{{ route('vehicle-dt') }}"
                   class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Registration No</th>
                        <th>Route</th>
                        <th>Capacity</th>
                        <th>Driver</th>
                        <th>Active</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="vehicle-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold font-3xl" id="exampleModalLabel">
                        <span id="vehicle-modal-title">Register</span> Vehicle</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <form action="{{ url('vehicle') }}" method="post" id="vehicle-form">
                        @csrf
                        <input type="hidden" name="_method" value="PUT" id="vehicle-spoof-input" disabled/>
                        <div class="modal-body">
                            <!-- /.row-->

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="registration_no" class="control-label">Registration No</label>
                                        <input class="form-control font-weight-bold text-dark" id="registration_no" type="text" name="registration_no"
                                               placeholder="Enter the Registration No" required>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="capacity" class="control-label">Capacity</label>
                                        <input class="form-control font-weight-bold text-dark" id="capacity" type="number" name="capacity"
                                               placeholder="Enter the Capacity" max="50" min="1" required>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                    <label class="control-label" for="route_id">Route</label>
                                    <select class="form-control" name="route_id" id="route_id" required>
                                        <option value="">--Select--</option>
                                        @if(count( $routes))
                                            @foreach($routes as $route)
                                                <option value="{{ $route->id }}">{{ $route->from.' -  '.$route->to}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="status">Status</label>
                                        <select class="form-control" name="active" id="active" required>
                                            <option value="0">Active</option>
                                            <option value="1">Inactive</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="driver_id">Driver</label>
                                        <select class="form-control" name="driver_id" id="driver_id" required>
                                            <option value=""></option>
                                            @if(count($drivers))
                                                @foreach($drivers as $driver)
                                                    <option value="{{ $driver->driver->id }}">
                                                        {{ $driver->first_name. ' ' . $driver->middle_name . ' ' . $driver->surname
                                                         . ' -  '.$driver->driver->membership_no }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row-->
                        </div>
                        <input type="hidden" name="id" id="id"/>
                        <div class="modal-footer">
                            <button class="btn btn-success">
                                <i class="fa fa-dot-circle-o"></i> Submit</button>
                            <button class="btn btn-danger" type="reset">
                                <i class="fa fa-ban"></i> Reset</button>
                            <button class="btn btn-default" type="button" data-dismiss="modal">
                                <i class="fa fa-close"></i> Close</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="import-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold font-3xl" id="exampleModalLabel">
                        <span id="vehicle-modal-title">Import</span> Vehicles from Excel File</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('import-vehicles') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="drivers-import" class="font-weight-bold">Upload Excel File</label>
                                    <input type="file" accept=".xls,.xlsx" class="form-control" name="vehicles_import" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control" name="id" id="id"/>
                    <div class="modal-footer">
                        <button class="btn btn-success">
                            <i class="fa fa-dot-circle-o"></i> Submit</button>
                        <button class="btn btn-danger" type="reset">
                            <i class="fa fa-ban"></i> Reset</button>
                        <button class="btn btn-default" type="button" data-dismiss="modal">
                            <i class="fa fa-close"></i> Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.child-layouts.datatables')
@section('title', 'Register a Route')


@push('my-js')
    <script src="{{ asset('js/routes.js') }}"></script>
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item active">Manage Routes</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header pt-2">
            <strong>Manage Routes</strong>
        </div>
        <div class="card-body">
            <div class="mb-3">
                <button class="btn btn-sm btn-primary" id="add-route-btn" data-toggle="modal" data-target="#route-modal" >
                    <i class="fa fa-plus"></i> Register Route</button>
            </div>
            @include('layouts.common.success')
            @include('layouts.common.warnings')
            @include('layouts.common.warning')
            <table id="route-dt" source="{{ route('route-dt') }}"
                   class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="route-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold font-3xl" id="exampleModalLabel">
                        <span id="route-modal-title">Register</span> Route</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <form action="{{ url('route') }}" method="post" id="route-form" >
                        @csrf
                        <input type="hidden" name="_method" value="PUT" id="route-spoof-input" disabled/>
                        <div class="modal-body">
                            {{--<strong class="font-2xl">Company Info</strong>--}}
                            <!-- /.row-->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="from" class="font-weight-bold">From</label>
                                        <input class="form-control font-weight-bold text-dark" id="from" type="text" name="from"
                                               placeholder="Enter the start point" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="to" class="font-weight-bold">To</label>
                                        <input class="form-control font-weight-bold text-dark" id="to" type="text" name="to"
                                               placeholder="Enter the destination">
                                    </div>
                                </div>
                            </div>
                            <!-- /.row-->
                            {{--<strong class="font-2xl">Company Admin</strong>--}}
                        </div>
                        <input type="hidden" name="id" id="id"/>
                        <div class="modal-footer">
                            <button class="btn btn-success">
                                <i class="fa fa-dot-circle-o"></i> Submit</button>
                            <button class="btn btn-danger" type="reset">
                                <i class="fa fa-ban"></i> Reset</button>
                            <button class="btn btn-default" type="button" data-dismiss="modal">
                                <i class="fa fa-close"></i> Close</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
@endsection
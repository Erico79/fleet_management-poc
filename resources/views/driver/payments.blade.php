@extends('layouts.child-layouts.datatables')
@section('title', 'Payments')


@push('my-js')
    <script src="{{ asset('js/driver/payments.js') }}"></script>
@endpush

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item active">Payments</li>
@endsection

@section('content')
    <div class="card">
        <div class="card-header pt-2">
            <strong>Payments</strong>
        </div>
        <div class="card-body">
            @include('layouts.common.success')
            @include('layouts.common.warnings')
            @include('layouts.common.warning')
            <table id="driver-payments-dt" source="{{ route('driver-payments-dt') }}"
                   class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Vehicle Registration No</th>
                    <th>Amount</th>
                    <th>Customer Phone No</th>
                    <th>Business Short Code</th>
                    <th>Transaction Time</th>
                    <th>Transaction Type</th>
                    <th>Transaction ID</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
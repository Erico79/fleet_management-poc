@extends('layouts.child-layouts.datatables')
@section('title', 'Dashboard')

@section('breadcrumb')
    <li class="breadcrumb-item active">Home</li>
@endsection

@push('css')
    <style>
        .card:hover {
            box-shadow: 10px 10px 10px #ccc;
        }

        .route-title * {
            font-size: large;
            text-align: left !important;
        }

        .route-title {
            cursor: pointer;
        }

        .chart-wrapper {
            width: 100%;
        }
    </style>
@endpush

@push('js')
    <script src="{{ asset('vendors/chart.js/js/Chart.min.js') }}"></script>
    <script src="{{ asset('js/dashboard.js') }}"></script>
@endpush

@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <h1>Welcome Driver.</h1>
        </div>
    </div>
@endsection

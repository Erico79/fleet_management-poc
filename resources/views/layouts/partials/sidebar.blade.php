<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            @if(count($menu))
                @foreach($menu as $item)
                    @if (isset($item['url']))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url($item['url']) }}">
                                <i class="nav-icon {{ $item['icon'] }}"></i> {{ $item['title'] }}
                            </a>
                        </li>
                    @else
                        <li class="nav-title">{{ $item['title'] }}</li>
                    @endif
                @endforeach
            @endif
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
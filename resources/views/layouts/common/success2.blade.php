@if (session()->has('success2'))
    <div class="alert alert-success">
        <button class="close" data-dismiss="alert">&times;</button>
        <strong>Success!</strong> {{ session()->get('success2') }}
    </div>
@endif
<?php
/**
 * Created by PhpStorm.
 * User: savannabits
 * Date: 7/19/18
 * Time: 11:37 AM
 */
?>
{{--<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">--}}
<link href="{{asset('assets1/css/bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css">
{{--<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>--}}
@stack('styles')
{{--<script src="{{asset('assets/js/core/jquery.min.js')}}"></script>--}}
{{--<script src="{{asset('assets/js/core/popper.min.js')}}"></script>--}}
{{--<script src="{{asset('assets/js/core/bootstrap-material-design.min.js')}}"></script>--}}
<!------ Include the above in your HEAD tag ---------->
@stack('js')
<div class="container">
    <div class="row">

        <div class="receipt-main col-xs-10 col-sm-10 col-md-6 col-xs-offset-1 col-sm-offset-1 col-md-offset-3">
            <div class="row">
                <div class="receipt-header">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="receipt-left">
                            <img class="img-responsive" alt="iamgurdeeposahan" src="http://bootsnipp.com/img/avatars/bcf1c0d13e5500875fdd5a7e8ad9752ee16e7462.jpg" style="width: 71px; border-radius: 43px;">
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                        <div class="receipt-right">
                            <h5>TechiTouch.</h5>
                            <p>+91 12345-6789 <i class="fa fa-phone"></i></p>
                            <p>info@gmail.com <i class="fa fa-envelope-o"></i></p>
                            <p>Australia <i class="fa fa-location-arrow"></i></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="receipt-header receipt-header-mid">
                    <div class="col-xs-8 col-sm-8 col-md-8 text-left">
                        <div class="receipt-right">
                            <h5>Gurdeep Singh <small>  |   Lucky Number : 156</small></h5>
                            <p><b>Mobile :</b> +91 12345-6789</p>
                            <p><b>Email :</b> info@gmail.com</p>
                            <p><b>Address :</b> Australia</p>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="receipt-left">
                            <h1>Receipt</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                @yield('body')
            </div>

            <div class="row">
                <div class="receipt-header receipt-header-mid receipt-footer">
                    <div class="col-xs-8 col-sm-8 col-md-8 text-left">
                        <div class="receipt-right">
                            <p><b>Date :</b> 15 Aug 2016</p>
                            <h5 style="color: rgb(140, 140, 140);">Thank you for your business!</h5>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4">
                        <div class="receipt-left">
                            <h1>Signature</h1>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
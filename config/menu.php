<?php
/**
 * Created by PhpStorm.
 * Date: 9/9/18
 * Time: 2:04 AM
 */
return [
    'systemadmin' => [
        [
            'url' => 'home',
            'title' => 'Dashboard',
            'parent' => false,
            'icon' => 'icon-speedometer'
        ],
        [
            'title' => 'Registration'
        ],
        [
            'url' => 'companies',
            'title' => 'Companies',
            'parent' => false,
            'icon' => 'icon-list'
        ],

    ],
    'companyadmin' => [
        [
            'url' => 'home',
            'title' => 'Dashboard',
            'parent' => false,
            'icon' => 'icon-speedometer'
        ],
        [
            'title' => 'Registration'
        ],
        [
            'url' => 'company-admins',
            'title' => 'Company Admins',
            'parent' => false,
            'icon' => 'fa fa-users'
        ],
        [
            'url' => 'routes',
            'title' => 'Routes',
            'parent' => false,
            'icon' => 'fa fa-road'
        ],
        [
            'url' => 'drivers',
            'title' => 'Drivers',
            'parent' => false,
            'icon' => 'fa fa-users'
        ],
        [
            'url' => 'vehicles',
            'title' => 'Vehicles',
            'parent' => false,
            'icon' => 'icon-list'
        ],
    ],
    'driver' => [
        [
            'url' => 'home',
            'title' => 'Dashboard',
            'parent' => false,
            'icon' => 'icon-speedometer'
        ],
        [
            'title' => 'Reports'
        ],
        [
            'url' => 'driver-payments',
            'title' => 'Payments',
            'parent' => false,
            'icon' => 'icon-list'
        ],
    ]
];
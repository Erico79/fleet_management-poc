$(document).ready(function() {
    var profileSpoofInput = $('#profile-spoof-input'),
        imagePic = $('img.profile-pic'),
        profileModal = $('#profile-modal'),
        editProfileBtn = $('#edit-profile-btn'),
        profileUrl = editProfileBtn.attr('profile-url');

    // edit   profile
    editProfileBtn.on('click', function() {
        var profileBtn = $(this);
        var profile_id = profileBtn.attr('pr-id'),
            profileForm = $('#profile-form');

        if (profile_id != '') {
            $.ajax({
                url: profileBtn.attr('source'),
                type: 'get',
                dataType: 'json',
                beforeSend: function () {
                    profileSpoofInput.removeAttr('disabled');
                    profileModal.find('form').addClass('block-ui');
                },
                success: function (data) {
                    profileModal.find('form').removeClass('block-ui');
                    // populate the modal fields using data from the server
                    $.each(data, function(i, value) {
                        var formInput = $('input[name='+ i + ']');

                        if (formInput.hasClass('form-control')) {
                            formInput.val(value);
                        } else if (formInput.hasClass('file-upload')) {
                            if (data['photo']) {
                                var fileData = JSON.parse(value);
                                imagePic.attr('src', fileData['asset_path']);
                            } else {
                                imagePic.attr('src', imagePic.attr('default-src'));
                            }
                        }

                    });
                    // set the update url
                    var action = profileForm.attr('action');
                    action = profileForm.attr('default-action') + '/' + profile_id;
                    profileForm.attr('action', action);

                    // open the modal
                    profileModal.modal('show');
                }
            });
        }
    });

    $('#profile-form').on('submit', function(e) {
        e.preventDefault();
        var form = $(this),
            html = '',
            errors;

        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,
            dataType: 'json',
            beforeSend: function() {
                profileModal.find('form').addClass('block-ui');
            },
            success: function(data) {
                profileModal.find('form').removeClass('block-ui');

                if (data['success']) {
                    resetForm(form);
                    profileModal.modal('hide');
                    swalWithBootstrapButtons('Success!', data['message'], 'success');
                    location.href = profileUrl;
                } else {
                    errors = data['errors'];

                    for(var x in errors) {
                        html += '<p class="font-weight-bold small text-danger">' + errors[x] + '</p>';
                    }

                    html += '</ul>';
                    swal({
                        title: 'Oops...',
                        type: 'error',
                        html: html,
                        showCloseButton: true,
                        focusConfirm: false,
                        cancelButtonText: 'Ok'
                    });
                }
            }
        });
    });


});

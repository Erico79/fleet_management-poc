var myModal = $('#the-modal'),
    spoofInput = $('#spoof-input'),
    companyInfo = $('#company-form');

$(document).ready(function() {
    // initialize dt
    var theTable = $('#companies-dt');
    var CompanyDT = theTable.DataTable({
        serverSide: true,
        processing: true,
        ajax: theTable.attr('source'),
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'company_no', name: 'company_no'},
            {data: 'physical_address', name: 'physical_address'},
            {data: 'postal_address', name: 'postal_address'},
            {data: 'telephone_no', name: 'telephone_no'},
            {data: 'email', name: 'email'},
            {data: 'actions', name: 'actions'},
        ],
        order: [[0, 'desc']]
    });

    $('#add-btn').on('click', function() {
        myModal.find('#form-title').text('Register');
        spoofInput.attr('disabled', 'disabled');
        companyInfo.find('input.form-control').val('');
        companyInfo.attr('action', companyInfo.attr('add-action'));
    });

    $('#company-form').on('submit', function(e) {
        e.preventDefault();
        var form = $(this),
            html = '',
            errors;

        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            dataType: 'json',
            beforeSend: function() {
                myModal.find('form').addClass('block-ui');
            },
            success: function(data) {
                myModal.find('form').removeClass('block-ui');

                if (data['success']) {
                    resetForm(form);
                    myModal.modal('hide');
                    swalWithBootstrapButtons('Success!', data['message'], 'success');
                    CompanyDT.ajax.reload()
                } else {
                    errors = data['errors'];

                    for(var x in errors) {
                        html += '<p class="font-weight-bold small text-danger">' + errors[x] + '</p>';
                    }

                    html += '</ul>';
                    swal({
                        title: 'Oops...',
                        type: 'error',
                        html: html,
                        showCloseButton: true,
                        focusConfirm: false,
                        cancelButtonText: 'Ok'
                    });
                }
            }
        });
    });

    $(document).on('click', '.co-edit-btn', function() {
        var editBtn = $(this);
        myModal.find('#form-title').text('Update');
        spoofInput.removeAttr('disabled');
        companyInfo.attr('action', companyInfo.attr('edit-action'));

        $.ajax({
            url: editBtn.attr('source'),
            type: 'get',
            dataType: 'json',
            beforeSend: function() {
                myModal.find('form').addClass('block-ui');
            },
            success: function(data) {
                myModal.find('form').removeClass('block-ui');

                $.each(data, function(i, value) {
                    $('input[name='+ i +']').val(value);
                });

                myModal.modal('show');
            }
        });
    });

    $(document).on('click', '.co-del-btn', function() {
        var btn = $(this);

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: btn.attr('source'),
                    type: 'delete',
                    data: { _token: $('input[name=_token]:first').val() },
                    dataType: 'json',
                    success: function(data) {
                        if (data['success']) {
                            swalWithBootstrapButtons('Success!', data['message'], 'success');
                            CompanyDT.ajax.reload();
                        } else {
                            swalWithBootstrapButtons('Success!', data['message'], 'danger');
                        }
                    }
                })
            }
        });
    });
});
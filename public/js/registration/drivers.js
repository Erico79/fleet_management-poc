$(document).ready(function() {
    // initialize dt
    var theTable = $('#driver-dt'),
         imagePic = $('img.profile-pic');;
    var driverDT = theTable.DataTable({
        processing: true, // loading icon
        serverSide: true, // this means the datatable is no longer client side
        ajax: theTable.attr('source'),
        columns: [ // datatable columns
            {data: 'id', name: 'id'},
            {data: 'driver_name', name: 'driver_name'},
            {data: 'membership_no', name: 'membership_no'},
            {data: 'id_no', name: 'id_no'},
            {data: 'phone_no', name: 'phone_no'},
            {data: 'email', name: 'email'},
            {data: 'actions', name: 'actions'}
        ],
        order: [[0, 'desc']]
    });

    var driverModalTitle = $('#driver-modal-title'),
        driverSpoofInput = $('#driver-spoof-input'),
        driverForm = $('#driver-form');
    var driverModal = $('#driver-modal');

// add  driver
    $('#add-driver-btn').on('click', function () {
        driverModal. find('#driver-modal-title').text('Register');
        driverSpoofInput.attr('disabled', 'disabled');
        driverForm.find('#driver-title').text('Register');
        driverForm.find('input.form-control').val('');
        driverForm.attr('action', driverForm.attr('source'));
        imagePic.attr('src', imagePic.attr('default-src'));
    });
    // edit   driver
    $(document).on('click', '.edit-driver-btn', function () {
        var driverBtn = $(this);
        var driver_id = driverBtn.attr('dr-id'),
            driverForm = $('#driver-form');

        if (driver_id != '') {
            $.ajax({
                url: driverBtn.attr('source'),
                type: 'get',
                dataType: 'json',
                beforeSend: function () {
                    driverModalTitle.text('Update');
                    driverSpoofInput.removeAttr('disabled');
                    driverModal.find('form').addClass('block-ui');
                },
                success: function (data) {
                    driverModal.find('form').removeClass('block-ui');
                    // populate the modal fields using data from the server
                    $.each(data, function(i, value) {
                        var formInput = $('input[name='+ i + ']');

                        if (formInput.hasClass('form-control')) {
                            formInput.val(value);
                        } else if (formInput.hasClass('file-upload')) {
                            if (data['photo']) {
                                var fileData = JSON.parse(value);
                                imagePic.attr('src', fileData['asset_path']);
                            } else {
                                imagePic.attr('src', imagePic.attr('default-src'));
                            }
                        }

                    });
                    // set the update url
                    var action = driverForm.attr('default-action');
                    action = action + '/' + driver_id;
                    driverForm.attr('action', action);

                    // open the modal
                    driverModal.modal('show');
                }
            });
        }
    });

    $(document).on('submit', '.del_driver_form', function () {
        if (confirm('Are you sure you want to delete the  driver')) {
            return true;
        }
        return false;
    });

});

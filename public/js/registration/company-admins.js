var admnModal = $('#admn-modal'),
    spoofInput = $('#spoof-admin-input'),
    admnInfo = $('#company-admin-form'),
    imagePic = $('img.profile-pic');

$(document).ready(function() {
    // initialize dt
    var theTable = $('#company-admins-dt');
    var CompanyAdminDT = theTable.DataTable({
        serverSide: true,
        processing: true,
        ajax: theTable.attr('source'),
        columns: [
            {data: 'id', name: 'id'},
            {data: 'first_name', name: 'first_name'},
            {data: 'middle_name', name: 'middle_name'},
            {data: 'surname', name: 'surname'},
            {data: 'email', name: 'email'},
            {data: 'phone_no', name: 'phone_no'},
            {data: 'id_no', name: 'id_no'},
            {data: 'actions', name: 'actions'},
        ],
        order: [[0, 'desc']]
    });

    $('#add-admn-btn').on('click', function() {
        admnModal.find('#admn-title').text('Register');
        spoofInput.attr('disabled', 'disabled');
        admnInfo.find('input.form-control').val('');
        admnInfo.attr('action', admnInfo.attr('add-action'));
        imagePic.attr('src', imagePic.attr('default-src'));
    });

    $('#company-admin-form').on('submit', function(e) {
        e.preventDefault();
        var form = $(this),
            html = '',
            errors;

        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,
            dataType: 'json',
            beforeSend: function() {
                admnModal.find('form').addClass('block-ui');
            },
            success: function(data) {
                admnModal.find('form').removeClass('block-ui');

                if (data['success']) {
                    resetForm(form);
                    admnModal.modal('hide');
                    swalWithBootstrapButtons('Success!', data['message'], 'success');
                    CompanyAdminDT.ajax.reload()
                } else {
                    errors = data['errors'];

                    for(var x in errors) {
                        html += '<p class="font-weight-bold small text-danger">' + errors[x] + '</p>';
                    }

                    html += '</ul>';
                    swal({
                        title: 'Oops...',
                        type: 'error',
                        html: html,
                        showCloseButton: true,
                        focusConfirm: false,
                        cancelButtonText: 'Ok'
                    });
                }
            }
        });
    });

    $(document).on('click', '.ad-edit-btn', function() {
        var editBtn = $(this);
        admnModal.find('#admn-title').text('update');
        spoofInput.removeAttr('disabled');
        admnInfo.attr('action', admnInfo.attr('edit-admin-action'));

        $.ajax({
            url: editBtn.attr('source'),
            type: 'get',
            dataType: 'json',
            beforeSend: function() {
                admnModal.find('form').addClass('block-ui');
            },
            success: function(data) {
                admnModal.find('form').removeClass('block-ui');

                $.each(data, function(i, value) {
                    var formInput = $('input[name='+ i + ']');

                    if (formInput.hasClass('form-control')) {
                        formInput.val(value);
                    } else if (formInput.hasClass('file-upload')) {
                        if (data['photo']) {
                            var fileData = JSON.parse(value);
                            imagePic.attr('src', fileData['asset_path']);
                        } else {
                            imagePic.attr('src', imagePic.attr('default-src'));
                        }
                    }

                });

                admnModal.modal('show');
            }
        });
    });

    $(document).on('click', '.ad-del-btn', function() {
        var btn = $(this);

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: btn.attr('source'),
                    type: 'delete',
                    data: { _token: $('input[name=_token]:first').val() },
                    dataType: 'json',
                    success: function(data) {
                        if (data['success']) {
                            swalWithBootstrapButtons('Success!', data['message'], 'success');
                            CompanyAdminDT.ajax.reload();
                        } else {
                            swalWithBootstrapButtons('Success!', data['message'], 'danger');
                        }
                    }
                })
            }
        });
    });
} );
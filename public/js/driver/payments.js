$(function() {
    // initialize dt
    var myTable = $('#driver-payments-dt');
    var driverPaymentsDT = myTable.DataTable({
        processing: true, // loading icon
        serverSide: true, // this means the datatable is no longer client side
        ajax: myTable.attr('source'),
        columns: [ // datatable columns
            {data: 'id', name: 'id'},
            {data: 'registration_no', name: 'registration_no'},
            {data: 'amount', name: 'amount'},
            {data: 'customer_phone_no', name: 'customer_phone_no'},
            {data: 'business_short_code', name: 'business_short_code'},
            {data: 'trans_time', name: 'trans_time'},
            {data: 'trans_type', name: 'trans_type'},
            {data: 'trans_id', name: 'trans_id'},
        ],
        order: [[0, 'desc']]
    });
});
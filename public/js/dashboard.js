$(function() {
    var start = moment().subtract(29, 'days');
    var end = moment();

    resetFilters();

    // date range picker
    function cb(start, end) {
        var from_date = start.format('YYYY-MM-DD 00:00:00'),
            to_date = end.format('YYYY-MM-DD 23:59:00'),
            date_range = {
                'from_date': from_date,
                'to_date': to_date,
            },
            filters = {};

        filters = getFilters();

        setDateRange(date_range, filters);
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    /**
     * DateRange picker
     * http://www.daterangepicker.com/#usage
     */
    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        var start = picker.startDate.format('YYYY-MM-DD HH:mm:ss'),
            end = picker.endDate.format('YYYY-MM-DD HH:mm:ss'),
            date_range = {
                'from_date': start,
                'to_date': end,
            },
            filters = JSON.parse(localStorage.getItem('filters'));

        setDateRange(date_range, filters);

        $('.card').each(function() {
            var card = $(this),
                isCardOpened = card.find('.card-body.show').length;

            if (isCardOpened) {
                var reg_no = card.find('input.search-reg-no').val();
                var newFilters = Object.assign({}, filters);
                if (reg_no != '')
                    newFilters['reg_no'] = reg_no;
                loadVehicleData(card, newFilters);
            }
        });
    });

    cb(start, end);

    $(document).on('show.bs.collapse', '.card', function () {
        resetFilters();
        var card = $(this),
            filters = JSON.parse(localStorage.getItem('filters'));

        loadVehicleData(card, filters);
    });

    $('.card').on('hide.bs.collapse', function() {
        $(this).find('.input-group').fadeOut('slow');
    });

    $('.search-reg-form').on('submit', function(e) {
        e.preventDefault();
        var searchRegForm = $(this),
            searchRegNo = searchRegForm.find('input').val(),
            card = searchRegForm.parents('.card');

        var filters = JSON.parse(localStorage.getItem('filters'));
        filters['reg_no'] = searchRegNo;
        loadVehicleData(card, filters);
    });

    $(document).on('change', '.no_of_records', function() {
        var selectRecordsField = $(this),
            card = selectRecordsField.parents('.card');;

        var filters = JSON.parse(localStorage.getItem('filters'));
        filters['no_of_records'] = selectRecordsField.val();
        localStorage.setItem('filters', JSON.stringify(filters));
        loadVehicleData(card, filters);
    });

    $(document).on('click', '.vehicle-section-div', function() {
        var vehicleSection = $(this),
            target = vehicleSection.attr('target'),
            vehicleId = vehicleSection.attr('vehicle-id'),
            dtSelector = '#vehicle-dt-' + vehicleId,
            filters = JSON.parse(localStorage.getItem('filters'));

        $(target).slideToggle('fast', function() {
            var hidden = $(target).is(":hidden");

            if (!hidden && vehicleId) {
                initTable(dtSelector, filters);
            }
        });
    });

    $(document).on('click', 'a.page-link', function(e) {
        e.preventDefault();
        var pageLink = $(this);
        var source = pageLink.attr('href');
        var card = pageLink.parents('.card');

        loadVehicleData(card, getFilters(), source);
    });
});

function loadVehicleData(card, filters, source) {
    var vehicleSection = card.find('.vehicle-section'),
        params = '';

    for (var x in filters) {
        params += x + "=" + filters[x] + "&";
    };

    if (source)
        var paginationSource = source + '&' + params;

    var source = paginationSource || vehicleSection.attr('vehicles-source') + '?' + params;

    $.ajax({
        url: source,
        type: 'get',
        dataType: 'html',
        beforeSend: function() {
            blockUI(card);
        },
        success: function(data) {
            vehicleSection.html(data);
            unblockUI(card);
        }
    });
}

function blockUI(card) {
    card.find('*').not('.loader').addClass('block-ui');
    card.find('.loader').show();
}

function unblockUI(card) {
    card.find('*').removeClass('block-ui');
    card.find('.loader').hide();
    card.find('.input-group').fadeIn('slow');
}

function initTable(dtSelector, filters) {
    var params = '';
    for (var x in filters) {
        params += x + "=" + filters[x] + "&";
    }
    if ( !$.fn.DataTable.isDataTable( dtSelector ) ) {
        $(dtSelector).DataTable({
            processing: true,
            serverside: true,
            ajax: $(dtSelector).attr('source') + '?' + params,
            columns: [
                {data: 'id', name: 'id'},
                {data: 'driver', name: 'driver'},
                {data: 'registration_no', name: 'registration_no'},
                {data: 'amount', name: 'amount'},
                {data: 'customer_phone_no', name: 'customer_phone_no'},
                {data: 'created_at', name: 'created_at'},
                {data: 'trans_id', name: 'trans_id'}
            ],
        });
    } else {
        var table = initialisedTable(dtSelector);
        var url = $(dtSelector).attr('source') + '?' + params;
        table.ajax.url(url).load();
    }
}

function initialisedTable (selector) {
    return $(selector).DataTable( {
        "retrieve": true
    } );
}

function setDateRange(date_range, filters) {
    if (filters) {
        for (var range in date_range) {
            filters[range] = date_range[range];
        }
        localStorage.setItem('filters', JSON.stringify(filters));
    } else {
        localStorage.setItem('filters', JSON.stringify(date_range));
    }

}

function getFilters() {
    if (localStorage.getItem('filters'))
        return JSON.parse(localStorage.getItem('filters'));
}

function resetFilters() {
    var filters;
    if (filters = localStorage.getItem('filters')) {
        var theFilters = JSON.parse(filters);
        if (theFilters.reg_no) delete theFilters.reg_no;
        localStorage.setItem('filters', JSON.stringify(theFilters));
    }
}
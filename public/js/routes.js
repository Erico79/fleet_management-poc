$(document).ready(function() {
    // initialize dt
    var routeTable = $('#route-dt');
    var routeDT = routeTable.DataTable({
        processing: true, // loading icon
        serverSide: true, // this means the datatable is no longer client side
        ajax: routeTable.attr('source'),
        columns: [ // datatable columns
            {data: 'id', name: 'id'},
            {data: 'from', name: 'from'},
            {data: 'to', name: 'to'},
            {data: 'actions', name: 'actions'}
        ],
        order: [[0, 'desc']]
    });

    var routeModalTitle = $('#route-modal-title'),
        routeSpoofInput = $('#route-spoof-input'),
        routeForm = $('#route-form'),
        routeModal = $('#route-modal');

// add  route
    $('#route-btn').on('click', function () {
        routeModalTitle. find('#route-modal-title').text('Register');
        routeSpoofInput.attr('disabled', 'disabled');
        routeForm.find('input.form-control').val('');
        routeForm.attr('action', routeForm.attr('source'));
    });
    // edit   route
    $(document).on('click', '.edit-route-btn', function () {
        var routeBtn = $(this);
        var route_id = routeBtn.attr('rt-id'),
            routeForm = $('#route-form');

        if (route_id != '') {
            $.ajax({
                url: routeBtn.attr('source'),
                type: 'get',
                dataType: 'json',
                beforeSend: function () {
                    routeModalTitle.text('Update');
                    routeSpoofInput.removeAttr('disabled');
                    routeModal.find('form').addClass('block-ui');
                },
                success: function (data) {
                        routeModal.find('form').removeClass('block-ui');
                    // populate the modal fields using data from the server
                    $.each(data, function (i, value) {
                        $('input[name=' + i + ']').val(value);
                    });

                    // set the update url
                    var action = routeForm.attr('action');
                    action = action + '/' + route_id;
                    routeForm.attr('action', action);
                    // open the modal
                    routeModal.modal('show');



                }
            });
        }
    });

    $(document).on('submit', '.del_route_form', function () {
        if (confirm('Are you sure you want to delete the  route')) {
            return true;
        }
        return false;
    });

});

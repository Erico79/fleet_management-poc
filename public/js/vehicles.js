$(document).ready(function() {
    // initialize dt
    var myTable = $('#vehicle-dt');
    var vehicleDT = myTable.DataTable({
        processing: true, // loading icon
        serverSide: true, // this means the datatable is no longer client side
        ajax: myTable.attr('source'),
        columns: [ // datatable columns
            {data: 'id', name: 'id'},
            {data: 'registration_no', name: 'registration_no'},
            {data: 'route_id', name: 'route_id'},
            {data: 'capacity', name: 'capacity'},
            {data: 'driver_id', name: 'driver_id'},
            {data: 'active', name: 'active'},
            {data: 'actions', name: 'actions'}
        ],
        order: [[0, 'desc']]
    });

    var vehicleModalTitle = $('#vehicle-modal-title'),
        vehicleSpoofInput = $('#vehicle-spoof-input'),
        vehicleForm = $('#vehicle-form'),
    vehicleModal = $('#vehicle-modal');

// add  vehicle
    $('#vehicle-btn').on('click', function () {
        vehicleModal. find('#vehicle-modal-title').text('Register');
        vehicleSpoofInput.attr('disabled', 'disabled');
        find('#vehicle-title').text('Register');
        vehicleForm.find('input.form-control,select').val('');
        vehicleForm.attr('action', vehicleForm.attr('source'));
    });
    // edit vehicle
    $(document).on('click', '.edit-vehicle-btn', function () {
        var vehicleBtn = $(this);
        var vehicle_id = vehicleBtn.attr('vh-id'),
            vehicleForm = $('#vehicle-form');

        if (vehicle_id != '') {
            $.ajax({
                url: vehicleBtn.attr('source'),
                type: 'get',
                dataType: 'json',
                beforeSend: function () {
                    vehicleModalTitle.text('Update');
                    vehicleSpoofInput.removeAttr('disabled');
                    vehicleModal.find('form').addClass('block-ui');
                },
                success: function (data) {
                    vehicleModal.find('form').removeClass('block-ui');
                    // populate the modal fields using data from the server
                    $('#registration_no').val(data['registration_no']);
                    $('#route_id').val(data['route_id']);
                    $('#capacity').val(data['capacity']);
                    $('#driver_id').val(data['driver_id']);
                    $('#active').val(data['active']);
                    $('#id').val(data['id']);
                    // set the update url
                    var action = vehicleForm.attr('action');
                    action = action + '/' + route_id;
                    vehicleForm.attr('action', action);

                    // open the modal
                    vehicleModal.modal('show');
                }
            });
        }
    });

    $(document).on('submit', '.del_vehicle_form', function () {
        if (confirm('Are you sure you want to delete the  driver')) {
            return true;
        }
        return false;
    });

});
